package de.hbrs.team37.parkhaus_simulation;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * common superclass for all servlets
 * groups all auxiliary common methods used in all servlets
 */
public abstract class AbstractParkhausServlet extends HttpServlet {

    //SERVLET INSTANCE VARIABLES
    protected CarPark carPark;
    protected String carType;
    protected List<Vehicle> carHistory = SingletonCarHistory.getInstance().getCarHistory();
    protected int initialCapacity;
    protected final Logger logger = Logger.getLogger(this.getName());

    //CONSTANTS
    private static final String CONTENT_TYPE = "text/html";
    public static final String HISTORY_SAVE_LOCATION = "./Data.txt";
    public static final String CURRENT_DATE = "./Date.txt";
    public static final String FALLBACK_TIME = "10:00";
    public static final int FALLBACK_DAY = 0;

    static Clock openingHours() {
        return new Clock(FALLBACK_TIME, "22:00", 1);
    }

    protected String config() {
        return this.max() + "," + carType + "," + carPark.isAvailable() + "," + Clock.getSimulationSpeed();
    }

    private String getName() {
        return "Bereich: " + carType;
    }

    public int max() {
        return this.carPark.getCapacity();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out;
        try {
            out = response.getWriter();
        } catch (IOException e) {
            logger.log(Level.WARNING, String.format("Could not print/get PrintWriter in doGet Servlet: %s", carType));
            out = new PrintWriter(new StringWriter());
        }
        String cmd = request.getParameter("cmd");
        logger.log(Level.INFO, String.format("%s requested: %s", cmd, request.getQueryString()));
        StatisticCollector statistics = new StatisticCollector(carHistory);

        switch (cmd) {
            case "config":
                // Max, open_from, open_to, delay, simulation_speed
                out.print(config());
                break;
            case "basePrices":
                out.print(MultitonVehicleTypes.getBasePrices());
                break;
            case "discounts":
                out.print(MultitonCustomerTypes.getAllDiscounts());
                break;
            case "average":
                if (!carHistory.isEmpty()) {
                    double averageCost = HelperClass.round(statistics.getStoredSum() / carHistory.size(), 2); //average cost
                    double averageTime = HelperClass.round(statistics.getStoredTime() / (double) carHistory.size(), 2);
                    out.print(averageCost + "," + averageTime);
                } else {
                    out.print("0,0");
                }
                break;
            case "getCurrentSum":
                out.print(statistics.getCurrentSum(carPark));
                break;
            case "getStoredSum":
                out.print(statistics.getStoredSum());
                break;
            case "getAllSum":
                out.print(statistics.getStoredSum() + statistics.getCurrentSum(carPark));
                break;
            case "getTimeSum":
                out.print(statistics.getStoredTime() + "m");
                break;
            case "cars":
                out.print(carPark.toString());
                break;
            case "currentTime":
                out.print(carPark.getClock().getDate());
                break;
            case "open":
                out.print(carPark.getClock().open());
                break;
            case "leavingCars":
                StringBuilder filteredCars = new StringBuilder();
                Vehicle[] filterNotPaid = carHistory.stream()
                        .filter(v -> (!v.getTicket().getIsPaid()) && (!v.getTicket().timeLeft.equals("Parking")))
                        .toArray(Vehicle[]::new);

                for (Vehicle v : filterNotPaid) {
                    filteredCars.append(v.toString()).append("|");
                }
                if (filteredCars.length() > 0)
                    filteredCars = new StringBuilder(filteredCars.substring(0, filteredCars.length() - 1));
                out.print(filteredCars);
                break;
            case "chartEarnings":
                String earnings = statistics.moneyFilter("customer", MultitonCustomerTypes.getNames())
                        + "||"
                        + statistics.moneyFilter("cartype", MultitonVehicleTypes.getNames())
                        + "||"
                        + statistics.moneyFilter("time_enter", new String[]{"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"})
                        + "||"
                        + statistics.moneyFilter("time_left", new String[]{"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"});
                out.print(earnings);
                break;
            case "chartNumbers":
                String numbers = statistics.numberFilter("customer", MultitonCustomerTypes.getNames())
                        + "||"
                        + statistics.numberFilter("cartype", MultitonVehicleTypes.getNames())
                        + "||"
                        + statistics.numberFilter("time_enter", new String[]{"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"})
                        + "||"
                        + statistics.numberFilter("time_left", new String[]{"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"});
                out.print(numbers);
                break;
            default:
                logger.log(Level.INFO, String.format("Invalid Command: %s", request.getQueryString()));
                out.print("");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

        String body = getBody(request);
        response.setContentType(CONTENT_TYPE);

        PrintWriter out;
        try {
            out = response.getWriter();
        } catch (IOException e) {
            logger.log(Level.WARNING, String.format("Could not print/get PrintWriter in doPost Servlet: %s", carType));
            out = new PrintWriter(new StringWriter());
        }
        logger.log(Level.INFO, body);
        String[] params = body.split(","); //[Event,platz]
        String event = params[0];
        switch (event) {
            case "enter":
                carEnter(out, params);
                break;
            case "randomEnter":
                String data = carPark.randomEnter(carType);
                out.print(data);
                logger.log(Level.INFO, data);
                break;
            case "leave":
                carLeave(out, params);
                break;
            case "randomLeave":
                randomLeave(out);
                break;
            case "payTicket":
                payTicket(out, params);
                break;
            case "changeCapacity":
                int newCap = Integer.parseInt(params[1]);
                out.print(carPark.setCapacity(newCap));
                break;
            case "decrease":
                out.print(carPark.decreaseCapacity());
                break;
            case "increase":
                out.print(carPark.increaseCapacity());
                break;
            case "disable":
                carPark.setAvailable(false);
                break;
            case "enable":
                carPark.setAvailable(true);
                break;
            case "changeBasePrice":
                String newPrice = params[1];
                try {
                    Objects.requireNonNull(MultitonVehicleTypes.getInstance(carType)).setBasePrice(Float.parseFloat(newPrice));
                    out.print(1);
                }catch (NumberFormatException | NullPointerException e) {
                    logger.log(Level.INFO, e.toString());
                    out.print(-1);
                }
                break;
            case "changeDiscounts":
                String[] newDiscounts = params[1].split("/");
                try {
                    MultitonCustomerTypes.getStandardInstance().setDiscount(Double.parseDouble(newDiscounts[0]));
                    MultitonCustomerTypes.getFamilyInstance().setDiscount(Double.parseDouble(newDiscounts[1]));
                    MultitonCustomerTypes.getBusinessInstance().setDiscount(Double.parseDouble(newDiscounts[2]));
                }catch (NumberFormatException e) {
                    logger.log(Level.INFO, e.toString());
                    out.print(-1);
                }
                break;
            case "simulationSpeed":
                String[] numbers;
                if (!params[1].contains("/"))
                    params[1] = params[1] + "/1";
                numbers = params[1].split("/");
                Clock.setSimulationSpeed(Double.parseDouble(numbers[0]) / Double.parseDouble(numbers[1]));
                break;
            case "clockDay":
                int day = Integer.parseInt(params[1]);
                carPark.getClock().setDay(day);
                break;
            case "reset":
                carPark.reset();
                break;
            case "resetAll":
                carPark.reset();
                SingletonCarHistory.getInstance().reset();
                MultitonCustomerTypes.reset();
                MultitonVehicleTypes.reset();
                Datamanager.saveHistory();
                Datamanager.loadHistory();
                destroy();
                try {
                    init();
                } catch (ServletException e) {
                    logger.log(Level.SEVERE, String.format("Could not initiate Servlet: %s", carType));
                }
                carPark.getClock().setDay(FALLBACK_DAY);
                carPark.getClock().setTime(FALLBACK_TIME);
                break;
            case "tomcat":
                if (getServletConfig() == null)
                    out.print("Servlet is offline");
                else
                    out.print(getServletConfig().getServletContext().getServerInfo()
                            + getServletConfig().getServletContext().getMajorVersion()
                            + getServletConfig().getServletContext().getMinorVersion());
                break;
            default:
                logger.log(Level.INFO, body);
        }
    }

    void payTicket(PrintWriter out, String[] params) {
        String ticketNr = params[1];
        Optional<Vehicle> veh = carHistory.stream()
                .filter(v -> v.getTicket().getHashcode().substring(10, 20).equalsIgnoreCase(ticketNr))
                .findFirst();
        if (veh.isPresent()) {
            veh.get().getTicket().setIsPaid(true);
            out.print("paid");
        } else {
            out.print("no such ticket");
        }
    }

    void randomLeave(PrintWriter out) {
        int slotID = carPark.randomLeave(); //Returns position of randomly selected car
        Vehicle vehicle;
        if (slotID != -1) {
            vehicle = carPark.leave(slotID);
            logger.log(Level.INFO, String.format("%d , %s", slotID, vehicle.toString()));
            out.print(slotID + "," + vehicle);
        } else {
            out.print(-1);
        }
    }

    void carLeave(PrintWriter out, String[] params) {
        int slotID = Integer.parseInt(params[1]); //try-catch
        Vehicle vehicle = carPark.leave(slotID);
        if (vehicle == null)
            out.print("-1");
        else
            out.print(slotID + "," + vehicle);
    }

    void carEnter(PrintWriter out, String[] params) {
        if (!carPark.isAvailable()) {
            out.print("notAvailable");
        } else {
            Vehicle newVehicle = AutoGenerator.generateCar(carType, params[2]);
            int slot = Integer.parseInt(params[1]);

            if (carPark.enter(newVehicle, slot))
                out.print(newVehicle.getCustomerType());
            else
                out.print("No parking space left");
        }
    }







    CarPark getCarPark() {
        return carPark;
    }

    List<Vehicle> getCarHistory() {
        return carHistory;
    }

    String getBody(HttpServletRequest request) {

        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, String.format("Could not print/get Body: %s", carType));
        }

        return stringBuilder.toString();
    }

    @Override
    public void destroy() {
        Datamanager.saveCarPark("./" + carType + ".txt", carPark);

        if (carType.equals(Objects.requireNonNull(MultitonVehicleTypes.getInstance("Car")).getName())) {
            try {
                File file = new File(AbstractParkhausServlet.CURRENT_DATE);
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                PrintWriter printWriter = new PrintWriter(fileOutputStream);
                printWriter.print(carPark.getClock().getDate());
                printWriter.close();
                Logger.getLogger("SUCCESS").log(Level.INFO, "Data saved successfully");
            } catch (IOException e) {
                Logger.getLogger("Save Time").log(Level.INFO, "Could not find file for time save");
            }
        }
    }
}