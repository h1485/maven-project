package de.hbrs.team37.parkhaus_simulation;

import java.security.SecureRandom;
import java.util.Arrays;

public class AutoGenerator {
    private static String[] customerTypes;
    private static final SecureRandom r = new SecureRandom();

    private AutoGenerator() {
    }

    public static Vehicle generateCar(String carType) {
        String foundType = "";
        MultitonVehicleTypes vehicleType = MultitonVehicleTypes.getInstance(carType);
        if(vehicleType != null ) foundType = vehicleType.getName();

        switch (foundType) {
            case "Truck":
                customerTypes = MultitonCustomerTypes.getWeightedNames(new int[]{2, 0, 3}); //Standarnd,Family,Business
                break;
            case "Bike":
                customerTypes = MultitonCustomerTypes.getWeightedNames(new int[]{3, 0, 2});
                break;
            default:
                customerTypes = MultitonCustomerTypes.getWeightedNames(new int[]{4, 2, 1});
        }
        return HelperClass.parseVehicle(carType, generateLicense(), customerTypes[r.nextInt(customerTypes.length)]);
    }

    public static Vehicle generateCar(String carType, String customer) {
        Vehicle vehicle = generateCar(carType);
        if (Arrays.toString(customerTypes).contains(customer))
            vehicle.setCustomerType(customer);
        return vehicle;
    }


    public static String generateLicense() {
        StringBuilder licenses = new StringBuilder();
        int maxLength = r.nextInt(6) + 3; //Maximal 8 Zeichen, mindestens 3 Zeichen(2 Buchstaben, 1 Zahl)
        int letterLength = r.nextInt(6 - 2) + 2; //Mindestens 2 Buchstaben, Maximal 5 Buchstaben)
        int numberLength = Math.min(maxLength - letterLength, 4);  //Mindestens 1 Zahl

        int interval = 'Z' - 'A' + 1;
        int firstLetters = r.nextInt(3) + 1;

        while ((letterLength - firstLetters) < 1)
            firstLetters = firstLetters - 1;

        for (int i = 0; i < firstLetters; i++) {
            char c = (char) ('A' + r.nextInt(interval));
            licenses.append(c);
        }

        licenses.append("-");

        int secondLetters = Math.min(r.nextInt(letterLength - firstLetters) + 1, 2);

        for (int i = 0; i < secondLetters; i++) {
            char c = (char) ('A' + r.nextInt(interval));
            licenses.append(c);
        }

        int randomnumber = r.nextInt(9) + 1; //Keine führende Nullen
        licenses.append("-").append(randomnumber);

        for (int i = 0; i < numberLength - 1; i++) {
            randomnumber = r.nextInt(10);
            licenses.append(randomnumber);
        }
        return licenses.toString();
    }

}
