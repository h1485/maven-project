package de.hbrs.team37.parkhaus_simulation;

public class Bike extends Vehicle{
    public Bike(String carID, String customerType){
        super(carID, customerType, MultitonVehicleTypes.getBikeInstance().getBasePrice()/60);
    }

    @Override
    public String toString(){
        return MultitonVehicleTypes.getBikeInstance().getName() + "," + super.toString();
    }
}
