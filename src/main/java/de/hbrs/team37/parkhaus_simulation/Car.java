package de.hbrs.team37.parkhaus_simulation;

public class Car extends Vehicle {

    public Car(String carID, String customerType){
        super(carID, customerType, MultitonVehicleTypes.getCarInstance().getBasePrice()/60);
    }

    @Override
    public String toString(){
        return MultitonVehicleTypes.getCarInstance().getName() + "," + super.toString();
    }
}
