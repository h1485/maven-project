package de.hbrs.team37.parkhaus_simulation;

import java.security.SecureRandom;
import java.util.*;

public class CarPark implements CarParkIF, Iterable<Vehicle> {

    private Vehicle[] vehicles;
    private int size = 0;
    private boolean isAvailable = true;
    private final Clock clock;
    private final List<Vehicle> carHistory = SingletonCarHistory.getInstance().getCarHistory();

    public CarPark(int capacity, Clock clock) {
        this.vehicles = new Vehicle[capacity];
        this.clock = clock;
    }

    public boolean enter(Vehicle vehicle, int platz) {
        if (!isAvailable ||
                platz >= vehicles.length ||
                platz < 0 ||
                vehicles[platz] != null) {
            return false;
        }
        vehicles[platz] = vehicle;
        carHistory.add(vehicle);
        vehicle.setEnterDate(clock.getDate());
        size++;
        return true;
    }


    public String randomEnter(String carType) {

        if (!isAvailable)
            return "notAvailable";

        if (size == vehicles.length) {
            return "-1";
        }

        Vehicle vehicle = AutoGenerator.generateCar(carType);

        int random = new SecureRandom().nextInt(vehicles.length);
        while (vehicles[random] != null) {
            random = new SecureRandom().nextInt(vehicles.length);
        }

        vehicles[random] = vehicle;
        carHistory.add(vehicle);
        vehicle.setEnterDate(clock.getDate());

        size++;
        return random + "," + vehicle.getCustomerType();
    }

    public Vehicle leave(int platz) {
        if (size == 0 || platz > vehicles.length || platz < 0 || vehicles[platz] == null) {
            return null;
        }
        Vehicle carToLeave = vehicles[platz];
        carToLeave.setTimeLeft(clock.getDate());
        vehicles[platz] = null;
        size--;
        return carToLeave;
    }

    public int randomLeave() {
        if (size == 0) {
            return -1;
        }

        int random = new SecureRandom().nextInt(vehicles.length);
        while (vehicles[random] == null)
            random = new SecureRandom().nextInt(vehicles.length);
        return random;
    }

    public int getCapacity() {
        return vehicles.length;
    }

    public int getTakenParkingSpaces() {
        return size;
    }

    public int getFreeParkingSpaces() {
        return vehicles.length - size;
    }

    public String toString() {
        StringBuilder cars = new StringBuilder();
        int counter = 0; //found cars
        int platz = -1; // index
        for (Vehicle vehicle : vehicles) {
            platz++;
            String car = "";
            if (vehicle != null) {
                counter++;
                car += platz + ",";
                car += vehicle.toString();
                if (counter < size)
                    car += "|";
            }
            cars.append(car);
        }
        return cars.toString();
    }

    public Vehicle[] getVehicles() {
        return vehicles;
    }

    public void reset() {
        vehicles = new Vehicle[vehicles.length];
        size = 0;
    }

    public int increaseCapacity() {
        vehicles = Arrays.copyOf(vehicles, vehicles.length + 1);
        return vehicles.length;
    }

    public int decreaseCapacity() {
        if (vehicles[vehicles.length - 1] == null)
            vehicles = Arrays.copyOf(vehicles, vehicles.length - 1);
        return vehicles.length;
    }

    public int setCapacity(int newCap) {
        vehicles = Arrays.copyOf(vehicles, newCap);
        return vehicles.length;
    }


    public Vehicle getPos(int slotID) {
        return vehicles[slotID];
    }

    public Clock getClock() {
        return clock;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean b) {
        this.isAvailable = b;
    }

    @Override
    public Iterator<Vehicle> iterator() {
        return new CarParkIterator(this.vehicles);
    }


    private class CarParkIterator implements Iterator<Vehicle> {
        Vehicle[] vehicles;
        int size = CarPark.this.size;
        int count = 0;
        int lastIndex = 0;

        public CarParkIterator(Vehicle[] vehicles) {
            this.vehicles = vehicles;
        }

        @Override
        public boolean hasNext() {
            return count < size;
        }

        @Override
        public Vehicle next() {
            Vehicle vehicle;
            while (hasNext()) {
                if (vehicles[lastIndex] != null) {
                    vehicle = vehicles[lastIndex++];
                    count++;
                    return vehicle;
                }
                lastIndex++;
            }
            throw new NoSuchElementException();
        }
    }

    public void setSize(int size) {
        this.size = size;
    }

}
