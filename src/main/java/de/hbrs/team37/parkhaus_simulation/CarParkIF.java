package de.hbrs.team37.parkhaus_simulation;

public interface CarParkIF {
    boolean enter(Vehicle vehicle, int i );
    String randomEnter(String carType);
    Vehicle leave(int i);
    int randomLeave();
    int getCapacity();
    int getTakenParkingSpaces();
    int getFreeParkingSpaces();
    Vehicle[] getVehicles();
    void reset();
    int increaseCapacity();
    int decreaseCapacity();
    Vehicle getPos(int pos);
    boolean isAvailable();
    void setAvailable(boolean b);
    String toString();
    int setCapacity(int newCap);
    Clock getClock();
}
