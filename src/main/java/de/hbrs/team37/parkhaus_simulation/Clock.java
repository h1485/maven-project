package de.hbrs.team37.parkhaus_simulation;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Clock extends Thread implements ClockIF {
    private int currentTime;
    private int day;
    private final int start;
    private final int end;
    private static boolean run = true;
    private static double simulationSpeed = 1; //1sec = 1m)

    public Clock(String start, String end, String current, int day, double simulationSpeedVal) {
        this.start = interpretTimeString(start);
        this.end = interpretTimeString(end);
        Clock.setSimulationSpeed(simulationSpeedVal);

        if (current.equals("") || day == -1) {
            try (BufferedReader reader = new BufferedReader(new FileReader(AbstractParkhausServlet.CURRENT_DATE))) {
                String time = reader.readLine();
                String[] timeSplit = time.split(" ");
                this.day = interpretDay(timeSplit[0]);
                currentTime = interpretTimeString(timeSplit[1]);
                Files.delete(Paths.get(AbstractParkhausServlet.CURRENT_DATE));
            } catch (Exception e) {
                currentTime = interpretTimeString(AbstractParkhausServlet.FALLBACK_TIME);
                this.day = AbstractParkhausServlet.FALLBACK_DAY;
            }
        } else {
            this.day = day % 7;
            this.currentTime = interpretTimeString(current);
        }
        initThread();
        this.start();
    }

    public Clock(String start, String end, String current) {
        this(start, end, current, 0, 1);
    }

    public Clock(String start, String end, String current, int day) {
        this(start, end, current, day, 1);
    }

    public Clock(String start, String end, int simulationSpeed) {
        this(start, end, "", -1, simulationSpeed);
    }

    @Override
    public void run() {
        while (run) {
            try {
                TimeUnit.MILLISECONDS.sleep(Math.round(1000.0 / simulationSpeed));
            } catch (InterruptedException e) {
                Logger.getLogger("Thread Clock").log(Level.WARNING, "Clock thread has been interrupted");
                stopThread();
                this.interrupt();
            }
            currentTime += 1;
            if (currentTime == 24 * 60) {
                day += 1;
                currentTime = 0;
            }
            day = day % 7;
        }
    }

    public static int interpretTimeString(String timeAsString) {
        try {
            String[] stringDeconstructed = timeAsString.split(":");
            int hour = Integer.parseInt(stringDeconstructed[0]);
            int min = Integer.parseInt(stringDeconstructed[1]);
            if (hour >= 24 || hour < 0 || min >= 60 || min < 0)
                throw new IllegalArgumentException("Ungültige Zeitangabe");

            return hour * 60 + min;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Ungültige Zeitangabe");
        }
    }

    public static String interpretTimeInt(int time) {
        int hour = 0;
        while (time >= 60) {
            hour += 1;
            time -= 60;
        }
        String ret = hour + ":" + time;
        if (time < 10)
            ret = hour + ":0" + time;
        return ret;
    }

    public boolean open() {
        if (start <= end) {
            return start < currentTime && currentTime < end;
        } else {
            return !(currentTime < start && currentTime > end);
        }
    }

    public String getOpenHour() {
        return interpretTimeInt(start) + "," + interpretTimeInt(end);
    }

    public String getCurrentTime() {
        return interpretTimeInt(currentTime);
    }

    public static double getSimulationSpeed() {
        return simulationSpeed;
    }

    public String getDay() {
        String[] days = {"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"};
        return days[day % 7];
    }

    public static int interpretDay(String day) {
        String[] days = {"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"};
        int i = 0;
        for (String e : days) {
            if (e.equals(day))
                return i;
            i++;
        }
        return 0;
    }

    public void setTime(String time) {
        currentTime = interpretTimeString(time);
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getDate() {
        return getDay() + " " + getCurrentTime();
    }

    public static void setSimulationSpeed(double speed) {
        Clock.simulationSpeed = speed;
    }

    public static void stopThread() {
        run = false;
    }

    public static void initThread() {
        run = true;
    }

    public static boolean getThreadState() {
        return run;
    }
}
