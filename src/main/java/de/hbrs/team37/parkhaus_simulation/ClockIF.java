package de.hbrs.team37.parkhaus_simulation;

public interface ClockIF {
    boolean open();
    String getOpenHour();
    String getCurrentTime();
    void setDay(int day);
    String getDate();
}
