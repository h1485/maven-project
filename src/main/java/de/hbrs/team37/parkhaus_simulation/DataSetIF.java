package de.hbrs.team37.parkhaus_simulation;

import javax.json.JsonObject;

public interface DataSetIF {
    void autoParameter();
    void setBorderWidth(double value);
    void setPointBorderColor(String color);
    void setPointBackgroundColor(String color);
    void setPointHoverBorderColor(String color);
    void setPointHoverBackgroundColor(String color);
    void setTitle(String title);
    void setFill(boolean fill);
    int[] size();
    void setBackgroundColor(String[] colors);
    void setBorderColor(String[] colors);
    void setTension(double value);
    JsonObject generateData();

}
