package de.hbrs.team37.parkhaus_simulation;

import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Datamanager {

    private  static final Logger logger = Logger.getLogger("Datamanager");

    private Datamanager() {
    }

    public static void saveHistory() {
        try {
            File file = new File(AbstractParkhausServlet.HISTORY_SAVE_LOCATION);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            PrintWriter printWriter = new PrintWriter(fileOutputStream);
            List<Vehicle> historyList = SingletonCarHistory.getInstance().getCarHistory();
            for (Vehicle vehicle : historyList) {
                printWriter.println(vehicle.storeFormat());
            }
            printWriter.close();
            fileOutputStream.close();
        } catch (IOException e) {
            logger.log(Level.WARNING, String.format("Could not print to file: %s", AbstractParkhausServlet.HISTORY_SAVE_LOCATION));
        }
    }

    public static void loadHistory() {
        try (BufferedReader reader = new BufferedReader(new FileReader(AbstractParkhausServlet.HISTORY_SAVE_LOCATION))) {
            List<Vehicle> vehicleList = SingletonCarHistory.getInstance().getCarHistory();
            String carString = reader.readLine();
            while (carString != null) {
                vehicleList.add(Datamanager.parseData(carString));
                carString = reader.readLine();
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, String.format("Could not read from file: %s", AbstractParkhausServlet.HISTORY_SAVE_LOCATION));
        }
    }

    public static void saveCarPark(String fileName, CarPark carPark) {
        try {
            File file = new File(fileName);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            PrintWriter printWriter = new PrintWriter(fileOutputStream);

            for (int i = 0; i < carPark.getCapacity(); i++) {
                Vehicle vehicle = carPark.getPos(i);
                if (vehicle != null)
                    printWriter.println(vehicle.storeFormat() + "," + i);
            }
            printWriter.close();
            fileOutputStream.close();
        } catch (IOException e) {
            logger.log(Level.WARNING, String.format("Could not print to file: %s", fileName));
        }
    }

    public static void loadCarPark(String fileName, CarPark carPark) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            Vehicle[] vehicles = carPark.getVehicles();
            String carString = reader.readLine();
            int number = 0;

            while (carString != null) {
                number++;
                String[] carDecomp = carString.split(",");
                int position = Integer.parseInt(carDecomp[carDecomp.length - 1]);
                vehicles[position] = parseData(carString);
                carString = reader.readLine();
            }
            carPark.setSize(number);
        } catch (IOException e) {
            logger.log(Level.WARNING, String.format("Could not read from file: %s", fileName));
        }


    }

    public static Vehicle parseData(String car) {
        String[] properties = car.split(",");
        Vehicle vehicle = HelperClass.parseVehicle(properties[0], properties[1], properties[2]);
        Ticket ticket = vehicle.getTicket();
        ticket.hashcode = "XXXXXXXXXX" + properties[3];
        ticket.timeEntered = properties[6];
        ticket.timeLeft = properties[7];
        ticket.start = new Date(Long.parseLong(properties[8]));
        ticket.end = new Date(Long.parseLong(properties[9]));
        return vehicle;
    }
}

