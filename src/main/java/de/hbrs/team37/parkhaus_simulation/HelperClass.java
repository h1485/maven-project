package de.hbrs.team37.parkhaus_simulation;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.*;

public final class HelperClass {

    private HelperClass() {
    }

    public static String toHex(byte[] bytes) {
        BigInteger bi = new BigInteger(1, bytes);
        return (String.format("%X", bi));
    }

    public static double round(double value, int places) {
        value = Math.round(value * Math.pow(10, places));
        return value / Math.pow(10, places);
    }

    //generates HSL Colors
    public static String[] generateColorRGB(int num, double alpha) {
        SecureRandom r = new SecureRandom();
        List<String> colors = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            int red = r.nextInt(256);
            int green = r.nextInt(256);
            int blue = r.nextInt(256);
            colors.add("rgba(" + red + "," + green + "," + blue + "," + alpha + ")");
        }
        String[] ret = new String[colors.size()];
        return colors.toArray(ret);
    }


    public static String[] generateColorHSLA(int num, double alpha, boolean recommended) {
        SecureRandom r = new SecureRandom();
        ArrayList<String> colors = new ArrayList<>();
        long hue = 0;
        double maxHues = 360.0 / num;
        for (int i = 0; i < num; i++) {
            hue = Math.round(maxHues + hue);
            int saturation = r.nextInt(61 - 20) + 20;
            int lightness = r.nextInt(61 - 20) + 20;
            if (recommended) {
                saturation = 100;
                lightness = 70;
            }
            colors.add("hsla(" + hue + "," + saturation + "%," + lightness + "%," + alpha + ")");
        }
        String[] ret = new String[colors.size()];
        return colors.toArray(ret);
    }

    public static String[] generateShadesRGB(String color, int numbers, boolean tint) {
        String rules = "rgba[(]\\d{1,3},\\d{1,3},\\d{1,3},\\d[.]\\d[)]";
        if (!color.matches(rules))
            return new String[0];
        double shadeScale = 1.0 / numbers;
        String[] shades = new String[numbers];
        String[] rawString = (color.replaceAll("[^\\d+,.]", "").split(","));
        double[] raw = Arrays.stream(rawString).mapToDouble(Double::parseDouble).toArray();

        if (tint) {
            for (int i = 0; i < raw.length; i++)
                raw[i] = 255 - raw[i];
        }

        for (int i = 0; i < numbers; i++) {
            String colorBuilder = "rgba(" + Math.round(raw[0] * shadeScale) + "," +
                    Math.round(raw[1] * shadeScale) + "," +
                    Math.round(raw[2] * shadeScale) + "," + rawString[3] + ")";
            shades[i] = colorBuilder;
            shadeScale += 1.0 / numbers;
        }
        return shades;
    }

    public static String[] changeAlpha(String[] colors, double alpha) {
        String rules = "(rgba[(]|hsla[(])((\\d+)%?,\\s?){3}(1|0.\\d+)\\s?[)]";

        for (int i = 0; i < colors.length; i++) {
            if (!colors[i].matches(rules)) {
                return colors;
            }
            String[] deconstructed = colors[i].split(",");
            String alphaElement = deconstructed[deconstructed.length - 1];
            deconstructed[deconstructed.length - 1] = deconstructed[deconstructed.length - 1].replace(alphaElement, Double.toString(alpha) + ")");
            colors[i] = Arrays.toString(deconstructed).replace("[", "").replace("]", "").replace(" ", "");
        }
        return colors;
    }

    public static String normalizeDuration(long duration) {
        String s = "";
        long days = duration / 1440;
        long hrs = (duration % 1440) / 60;
        long minutes = (duration % 1440) % 60;


        if (days != 0) s += days + "d ";
        if (hrs != 0) s += hrs + "h ";

        s += minutes + "min";

        return s;
    }

    //https://stackoverflow.com/questions/10174898/how-to-check-whether-a-given-string-is-valid-json-in-java
    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public static Vehicle parseVehicle(String type, String carID, String customerType){
        MultitonVehicleTypes vehicleType = MultitonVehicleTypes.getInstance(type);

        String typeName = vehicleType != null ? vehicleType.getName() : "not found";

        Vehicle vehicle;
        switch (typeName){
            case "Car":
                vehicle = new Car(carID, customerType);
                break;
            case "Truck":
                vehicle = new Truck(carID, customerType);
                break;
            case "Bike":
                vehicle = new Bike(carID, customerType);
                break;
            default:
                Logger.getLogger("VehicleType").log(Level.WARNING,"VehicleType not found basic car will be taken");
                vehicle = new Car(carID, customerType);
        }
        return vehicle;
    }
}
