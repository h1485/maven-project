package de.hbrs.team37.parkhaus_simulation;

import javax.json.*;
import javax.json.stream.JsonGenerator;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JsonChartGenerator implements JsonChartGeneratorIF {

    public class DataSet implements DataSetIF {

        private ArrayList<String> backgroundColor = new ArrayList<>();
        private ArrayList<String> borderColor = new ArrayList<>();
        private String title = "";
        private String pointBorderColor = "";
        private String pointBackgroundColor = "";
        private String pointHoverBorderColor = "";
        private String pointHoverBackgroundColor = "";
        private double[] data;
        private double[] y = {0};
        private double[] r = {3};
        private boolean fill = false;
        private double borderWidth = 0;
        private double tension = 0;


        public DataSet(double[] data, double[] y, double[] r) {
            this.data = data;
            if (r != null)
                this.r = r;
            if (y != null)
                this.y = y;
        }

        public void autoParameter() {
            String[] backgroundColors = HelperClass.generateColorHSLA(data.length, 0.5, true);
            setBackgroundColor(backgroundColors);
            String[] borderColors = HelperClass.changeAlpha(backgroundColors, 1);
            setBorderWidth(2);
            setTension(0.1);
            setBorderColor(borderColors);
            setPointBackgroundColor(borderColors[0]);
            setPointBorderColor("rgba(255,255,255,1)");
            setPointHoverBackgroundColor("rgba(255,255,255,1)");
            setPointHoverBorderColor(borderColors[0]);
        }

        public void setBorderWidth(double value) {
            borderWidth = value;
        }

        public void setPointBorderColor(String color) {
            pointBorderColor = color;
        }

        public void setPointBackgroundColor(String color) {
            pointBackgroundColor = color;
        }

        public void setPointHoverBorderColor(String color) {
            pointHoverBorderColor = color;
        }

        public void setPointHoverBackgroundColor(String color) {
            pointHoverBackgroundColor = color;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setFill(boolean fill) {
            this.fill = fill;
        }


        public int[] size() {
            int x = 0;

            if (data != null)
                x = data.length;
            return new int[]{x, y.length, r.length};
        }

        public void setBackgroundColor(String[] colors) {
            backgroundColor = new ArrayList<>(Arrays.asList(colors));
        }


        public void setBorderColor(String[] colors) {
            borderColor = new ArrayList<>(Arrays.asList(colors));
        }

        public void setTension(double value) {
            tension = value;
        }

        public JsonObject generateData() {

            JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder()
                    .add("label", title);

            //Scatter and Bubble Charts brauchen spezielle Datentypen
            if ((type.equals("scatter") || type.equals("bubble"))) {
                JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                JsonObjectBuilder pointBuilder = Json.createObjectBuilder();
                double rLocal = this.r[0];
                for (int i = 0; i < Math.min(data.length, y.length); i++) {
                    pointBuilder.add("x", data[i]).add("y", y[i]);
                    if (this.r.length == Math.min(data.length, y.length))
                        rLocal = this.r[i];
                    pointBuilder.add("r", rLocal);

                    arrayBuilder.add(pointBuilder);
                }
                jsonObjectBuilder.add("data", arrayBuilder);
            } else {
                JsonArrayBuilder values = Json.createArrayBuilder();
                for (double e : this.data)
                    values.add(e);
                jsonObjectBuilder.add("data", values);
            }

            jsonObjectBuilder.add("fill", fill);
            jsonObjectBuilder.add("borderWidth", borderWidth);
            jsonObjectBuilder.add("tension", tension);
            jsonObjectBuilder.add("pointBackgroundColor", pointBackgroundColor);
            jsonObjectBuilder.add("pointBorderColor", pointBorderColor);
            jsonObjectBuilder.add("pointHoverBackgroundColor", pointHoverBackgroundColor);
            jsonObjectBuilder.add("pointHoverBorderColor", pointHoverBorderColor);

            JsonArrayBuilder bgcolors = Json.createArrayBuilder();
            for (String bgcolor : backgroundColor)
                bgcolors.add(bgcolor);
            jsonObjectBuilder.add("backgroundColor", bgcolors);

            JsonArrayBuilder bcolors = Json.createArrayBuilder();
            for (String bcolor : borderColor)
                bcolors.add(bcolor);
            jsonObjectBuilder.add("borderColor", bcolors);

            return jsonObjectBuilder.build();
        }
    }


    private String[] labels;
    private ArrayList<DataSet> dataset;
    private String type = "bar";
    private String[] supportedTypes = {"line", "bar", "radar", "doughnut",
            "pie", "polarArea", "bubble", "scatter"};

    public JsonChartGenerator(String[] label) {
        this.labels = label;
        this.dataset = new ArrayList<>();
    }

    public JsonChartGenerator() {
        this(new String[]{});
    }

    public void addDataSet(DataSet data) {
        dataset.add(data);
    }

    public DataSet createDataSet(double[] data) {
        return new DataSet(data, null, null);
    }

    public DataSet createDataSet(double[] data, double[] y) {
        return new DataSet(data, y, null);
    }

    public DataSet createDataSet(double[] data, double[] y, double[] r) {
        return new DataSet(data, y, r);
    }

    public List<DataSet> getDataSets() {
        return dataset;
    }

    public DataSet getData(int pos) {
        return dataset.get(pos);
    }

    public boolean setType(String type) {
        if (Arrays.toString(supportedTypes).contains(type)) {
            this.type = type;
            return true;
        }
        Logger.getLogger("JsonChartGenerator").log(Level.INFO, "Chart Typ nicht gefunden. 'bar' wird ausgewählt.");
        return false;
    }

    public String generateChart(){
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();

        if (labels != null) {
            JsonArrayBuilder labelBuilder = Json.createArrayBuilder();
            for (String label : labels)
                labelBuilder.add(label);
            objectBuilder.add("labels", labelBuilder);
        }

        JsonArrayBuilder datasetBuilder = Json.createArrayBuilder();
        for (DataSet e : dataset)
            datasetBuilder.add(e.generateData());
        objectBuilder.add("datasets", datasetBuilder);

        JsonObject jsonData = objectBuilder.build();

        Map<String, Boolean> config = new HashMap<>();
        config.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory prettyWriter = Json.createWriterFactory(config);

        try (Writer writer = new StringWriter()) {
            prettyWriter.createWriter(writer).write(jsonData);

            return type + "|" + writer;
        } catch (IOException e) {
            Logger.getLogger("CharGenerator could not generate Chart.");
        }
        return "";
    }

}
