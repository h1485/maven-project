package de.hbrs.team37.parkhaus_simulation;

import java.io.IOException;
import java.util.List;

public interface JsonChartGeneratorIF {
    void addDataSet(JsonChartGenerator.DataSet data);
    JsonChartGenerator.DataSet createDataSet(double[] data);
    JsonChartGenerator.DataSet createDataSet(double[] data, double[] y);
    JsonChartGenerator.DataSet createDataSet(double[] data, double[] y, double[] r);
    List<JsonChartGenerator.DataSet> getDataSets();
    JsonChartGenerator.DataSet getData(int pos);
    boolean setType(String type);
    String generateChart() throws IOException;



}
