package de.hbrs.team37.parkhaus_simulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MultitonCustomerTypes implements MultitonCustomerTypesIF {
    private static final MultitonCustomerTypes[] customerTypes = new MultitonCustomerTypes[3];
    private static final MultitonCustomerTypes standardType =
            new MultitonCustomerTypes("Standard", 1.0);
    private static final MultitonCustomerTypes familyType =
            new MultitonCustomerTypes("Family", 0.7);
    private static final MultitonCustomerTypes businessType =
            new MultitonCustomerTypes("Business", 0.3);

    static {
        customerTypes[0] = standardType;
        customerTypes[1] = familyType;
        customerTypes[2] = businessType;
    }

    private final String name;
    private double discount;

    private MultitonCustomerTypes(String name, double discount) {
        this.name = name;
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }


    public static MultitonCustomerTypes getInstance(String value){
        for(MultitonCustomerTypes customerType : customerTypes){
            if(customerType.getName().equals(value))
                return customerType;
        }
        return standardType;
    }

    public static MultitonCustomerTypes getStandardInstance() {
        return standardType;
    }

    public static MultitonCustomerTypes getFamilyInstance() {
        return familyType;
    }

    public static MultitonCustomerTypes getBusinessInstance() {
        return businessType;
    }

    public static String getAllDiscounts() {
        return standardType.discount + "|"
                + familyType.discount + "|"
                + businessType.discount;
    }

    public static String[] getNames(){
        return Arrays.stream(customerTypes)
                .map(MultitonCustomerTypes::getName)
                .toArray(String[]::new);
    }

    public static String[] getWeightedNames(int[] weights){
        List<String> list = new ArrayList<>();
        for(int i = 0; i < weights.length; i++){
            for(int j = 0; j<weights[i]; j++){
                list.add(customerTypes[i].getName());
            }
        }
        return list.toArray(String[]::new);
    }

    public static String getCustomerRegex(){
        StringBuilder stringBuilder = new StringBuilder("(");
        for(MultitonCustomerTypes customer : customerTypes){
            stringBuilder.append(customer.getName()).append("|");
        }
        stringBuilder.append("\b)");
        return stringBuilder.toString();
    }

    public static void reset() {
        standardType.setDiscount(1);
        familyType.setDiscount(0.7);
        businessType.setDiscount(0.3);
    }
}
