package de.hbrs.team37.parkhaus_simulation;

public interface MultitonCustomerTypesIF {
    String getName();
    double getDiscount();
    void setDiscount(double discount);
}
