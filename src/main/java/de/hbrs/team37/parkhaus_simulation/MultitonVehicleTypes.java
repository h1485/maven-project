package de.hbrs.team37.parkhaus_simulation;

import java.util.Arrays;

public class MultitonVehicleTypes implements MultitonVehicleTypesIF{
    private static final MultitonVehicleTypes[] vehicleTypes = new MultitonVehicleTypes[3];
    private static final MultitonVehicleTypes typeCar =
            new MultitonVehicleTypes("Car", "./Car.txt", 20, 2.0);
    private static final MultitonVehicleTypes typeTruck =
            new MultitonVehicleTypes("Truck", "./Truck.txt", 15, 15.0);
    private static final MultitonVehicleTypes typeBike =
            new MultitonVehicleTypes("Bike", "./Bike.txt", 10, 1.0);

    static {
        vehicleTypes[0] = typeCar;
        vehicleTypes[1] = typeTruck;
        vehicleTypes[2] = typeBike;
    }

    private final String name;
    private final String path;
    private final int initialCapacity;
    private double basePrice;


    private MultitonVehicleTypes(String name, String path, int initialCapacity, double basePrice) {
        this.name = name;
        this.path = path;
        this.initialCapacity = initialCapacity;
        this.basePrice = basePrice;
    }

    public static MultitonVehicleTypes[] getAllInstances() {
        return vehicleTypes;
    }


    public static MultitonVehicleTypes getInstance(String type) {
       switch (type) {
           case "Car":
               return getCarInstance();
           case "Truck":
               return getTruckInstance();
           case "Bike":
               return getBikeInstance();
           default:
               return null;
       }
    }

    public static MultitonVehicleTypes getCarInstance() {
        return typeCar;
    }
    public static MultitonVehicleTypes getTruckInstance() {
        return typeTruck;
    }
    public static MultitonVehicleTypes getBikeInstance() {
        return typeBike;
    }

    public String getName() {
        return name;
    }
    public String getPath() {
        return path;
    }
    public int getInitialCapacity() {
        return initialCapacity;
    }
    public double getBasePrice() {
        return this.basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;

    }

    public static String getBasePrices() {
        return typeCar.getBasePrice() + "|"
                + typeTruck.getBasePrice() + "|"
                + typeBike.getBasePrice();
    }

    public static String[] getNames(){
        return Arrays.stream(vehicleTypes).map(MultitonVehicleTypes::getName).toArray(String[]::new);
    }

    public static String getVehicleTypesRegex(){
        StringBuilder stringBuilder = new StringBuilder("(");
        for(MultitonVehicleTypes vehicleType : vehicleTypes){
            stringBuilder.append(vehicleType.getName()).append("|");
        }
        stringBuilder.append("\b)");
        return stringBuilder.toString();
    }

    @Override
    public String toString() {
        return this.name;
    }

    public static void reset() {
        typeCar.setBasePrice(2.0);
        typeTruck.setBasePrice(15.0);
        typeBike.setBasePrice(1.0);
    }

}
