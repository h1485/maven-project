package de.hbrs.team37.parkhaus_simulation;

public interface MultitonVehicleTypesIF {
    String getName();
    String getPath();
    int getInitialCapacity();
    String toString();
}
