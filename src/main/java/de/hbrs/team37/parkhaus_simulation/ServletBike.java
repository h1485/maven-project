package de.hbrs.team37.parkhaus_simulation;

import javax.servlet.annotation.WebServlet;

@WebServlet(name = "ServletBike", value = "/Bike")
public class ServletBike extends AbstractParkhausServlet {

    public void init() {
        initialCapacity = MultitonVehicleTypes.getBikeInstance().getInitialCapacity();
        carPark = new CarPark(initialCapacity, openingHours());
        carType = MultitonVehicleTypes.getBikeInstance().getName();
        Datamanager.loadCarPark(MultitonVehicleTypes.getBikeInstance().getPath(), carPark);
    }

}