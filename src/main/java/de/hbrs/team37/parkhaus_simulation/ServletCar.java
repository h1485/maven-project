package de.hbrs.team37.parkhaus_simulation;

import javax.servlet.annotation.WebServlet;

@WebServlet(name = "ServletCar", value = "/Car")
public class ServletCar extends AbstractParkhausServlet {

    public void init() {
        initialCapacity = MultitonVehicleTypes.getCarInstance().getInitialCapacity();
        carPark = new CarPark(initialCapacity, openingHours());
        carType = MultitonVehicleTypes.getCarInstance().getName();
        Datamanager.loadCarPark(MultitonVehicleTypes.getCarInstance().getPath(), carPark);
    }

}