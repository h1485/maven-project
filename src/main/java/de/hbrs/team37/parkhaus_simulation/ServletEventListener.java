package de.hbrs.team37.parkhaus_simulation;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ServletEventListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event){
            Datamanager.loadHistory();
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        Clock.stopThread();
        Datamanager.saveHistory();
    }
}