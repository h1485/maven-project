package de.hbrs.team37.parkhaus_simulation;

import javax.servlet.annotation.WebServlet;

@WebServlet(name = "ServletTruck", value = "/Truck")
public class ServletTruck extends AbstractParkhausServlet {

    public void init() {
        initialCapacity = MultitonVehicleTypes.getTruckInstance().getInitialCapacity();
        carPark = new CarPark(initialCapacity, openingHours());
        carType = MultitonVehicleTypes.getTruckInstance().getName();
        Datamanager.loadCarPark(MultitonVehicleTypes.getTruckInstance().getPath(), carPark);
    }

}