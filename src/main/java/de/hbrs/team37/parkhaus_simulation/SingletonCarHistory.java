package de.hbrs.team37.parkhaus_simulation;

import java.util.ArrayList;
import java.util.List;

public class SingletonCarHistory implements SingletonCarHistoryIF {
    private List<Vehicle> carHistory;
    private static SingletonCarHistory singleton = null;

    private SingletonCarHistory() {
        carHistory = new ArrayList<>();
    }

    public static SingletonCarHistory getInstance() {
        if(singleton == null) singleton = new SingletonCarHistory();
        return singleton;
    }

    public List<Vehicle> getCarHistory(){
        return carHistory;
    }

    public void reset(){
        carHistory = new ArrayList<>();
    }
}
