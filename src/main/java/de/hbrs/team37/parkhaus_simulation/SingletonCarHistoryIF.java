package de.hbrs.team37.parkhaus_simulation;

import java.util.List;

public interface SingletonCarHistoryIF {
    List<Vehicle> getCarHistory();
    void reset();
}
