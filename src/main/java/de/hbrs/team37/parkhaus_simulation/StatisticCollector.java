package de.hbrs.team37.parkhaus_simulation;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class StatisticCollector implements StatisticCollectorIF {

    private List<Vehicle> data;
    private double[] generatedData;
    private static final String PARKING = "Parking";


    public StatisticCollector(List<Vehicle> arrayList) {
        data = arrayList;
    }

    private enum Features {
        CUSTOMER("customer", "Customer type"),
        CARTYPES("cartype", "Car type"),
        TIMEENTER("time_enter", "Day - Time entered"),
        TIMELEFT("time_left", "Day - Time left"),
        UKNOWN("default", "");

        private final String name;
        private final String title;

        Features(String features, String title) {
            this.name = features;
            this.title = title;
        }

        public static Features parseString(String value) {
            for (Features features : Features.values()) {
                if (features.name.equals(value))
                    return features;
            }
            return UKNOWN;
        }
    }

    public String moneyFilter(String featureType, String[] featuresToFilter) {

        String title = "";
        double[] values = new double[featuresToFilter.length];
        Features features = Features.parseString(featureType);
        switch (features) {
            case CUSTOMER:
                title = Features.CUSTOMER.title;
                for (int i = 0; i < values.length; i++) {
                    String feature = featuresToFilter[i];
                    values[i] = data.stream()
                            .filter(vehicle -> vehicle.getCustomerType().equals(feature) && !vehicle.getTicket().timeLeft.equals(PARKING))
                            .mapToDouble(Vehicle::getPrice).sum();
                }
                generatedData = values;
                break;
            case CARTYPES:
                title = Features.CARTYPES.title;
                for (int i = 0; i < values.length; i++) {
                    String feature = featuresToFilter[i];
                    values[i] = data.stream()
                            .filter(vehicle -> vehicle.toString().split(",")[0].equals(feature) && !vehicle.getTicket().timeLeft.equals(PARKING))
                            .mapToDouble(Vehicle::getPrice).sum();
                }
                generatedData = values;
                break;
            case TIMEENTER:
                title = Features.TIMEENTER.title;
                for (int i = 0; i < values.length; i++) {
                    String feature = featuresToFilter[i];
                    values[i] = data.stream()
                            .filter(vehicle -> vehicle.getTicket().timeEntered.split(" ")[0].equals(feature) && !vehicle.getTicket().timeLeft.equals(PARKING))
                            .mapToDouble(Vehicle::getPrice).sum();
                }
                generatedData = values;
                break;
            case TIMELEFT:
                title = Features.TIMELEFT.title;
                for (int i = 0; i < values.length; i++) {
                    String feature = featuresToFilter[i];
                    values[i] = data.stream()
                            .filter(vehicle -> vehicle.getTicket().timeLeft.split(" ")[0].equals(feature) && !vehicle.getTicket().timeLeft.equals(PARKING))
                            .mapToDouble(Vehicle::getPrice).sum();
                }
                generatedData = values;
                break;
            default:
                return "Invalid input";
        }


        JsonChartGenerator chart = new JsonChartGenerator(featuresToFilter);
        JsonChartGenerator.DataSet set = chart.createDataSet(values);
        set.autoParameter();
        set.setTitle("Income depending on: " + title);
        chart.addDataSet(set);
        return chart.generateChart();
    }


    public String numberFilter(String featureType, String[] featuresToFilter) {

        String title = "";
        double[] values = new double[featuresToFilter.length];
        Features features = Features.parseString(featureType);
        switch (features) {
            case CUSTOMER:
                title = Features.CUSTOMER.title;
                for (int i = 0; i < values.length; i++) {
                    String feature = featuresToFilter[i];
                    values[i] = data.stream().filter(vehicle -> vehicle.getCustomerType().equals(feature)).count();
                }
                generatedData = values;
                break;
            case CARTYPES:
                title = Features.CUSTOMER.title;
                for (int i = 0; i < values.length; i++) {
                    String feature = featuresToFilter[i];
                    values[i] = data.stream().filter(vehicle -> vehicle.toString().split(",")[0].equals(feature)).count();
                }
                generatedData = values;
                break;
            case TIMEENTER:
                title = Features.TIMEENTER.title;
                for (int i = 0; i < values.length; i++) {
                    String feature = featuresToFilter[i];
                    values[i] = data.stream().filter(vehicle -> vehicle.getTicket().timeEntered.split(" ")[0].equals(feature)).count();
                }
                generatedData = values;
                break;
            case TIMELEFT:
                title = Features.TIMELEFT.title;
                for (int i = 0; i < values.length; i++) {
                    String feature = featuresToFilter[i];
                    values[i] = data.stream().filter(vehicle -> vehicle.getTicket().timeLeft.split(" ")[0].equals(feature)).count();
                }
                generatedData = values;
                break;
            default:
                return "Invalid input";
        }


        JsonChartGenerator chart = new JsonChartGenerator(featuresToFilter);
        JsonChartGenerator.DataSet set = chart.createDataSet(values);
        set.autoParameter();
        set.setTitle("Number of occurrences depending on: " + title);
        chart.addDataSet(set);
        return chart.generateChart();
    }


    double getStoredSum() {
        double sum = data.stream()
                .filter(v -> !v.getTicket().timeLeft.equals(PARKING))
                .mapToDouble(Vehicle::getPrice)
                .sum();

        return HelperClass.round(sum, 2);
    }

    double getCurrentSum(CarPark carPark) {
        return HelperClass.round(Arrays.stream(carPark.getVehicles()).filter(Objects::nonNull).mapToDouble(Vehicle::getPrice).sum(), 2);
    }

    Long getStoredTime() {
        return data.stream()
                .map(Vehicle::getTicket)
                .mapToLong(Ticket::getTime)
                .sum();
    }

    public double[] getGeneratedData() {
        return generatedData;
    }

}

