package de.hbrs.team37.parkhaus_simulation;

import java.io.IOException;

public interface StatisticCollectorIF {
    String moneyFilter(String featureType, String[] featuresToFilter) throws IOException;
    String numberFilter(String featureType, String[] featuresToFilter) throws IOException;
    double[] getGeneratedData();
}
