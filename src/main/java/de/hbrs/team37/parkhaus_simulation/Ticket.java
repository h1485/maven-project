package de.hbrs.team37.parkhaus_simulation;
import org.mindrot.jbcrypt.BCrypt;
import java.util.Date;

public class Ticket implements TicketIF {

    Date start;
    Date end;
    String hashcode;
    String timeEntered = "";
    String timeLeft = "Parking";
    boolean isPaid = false;

    public Ticket(String carid) {
        this.start = new Date();
        String raw = carid + start;
        hashcode = BCrypt.hashpw(raw, BCrypt.gensalt());

    }
    public String getHashcode(){
        return hashcode;
    }

    public Date getStart(){
        return start;
    }

    public long getTime(){ //returns Time passed since the beginning in seconds
        if (end != null){
            return Math.round(((end.getTime()-start.getTime())/1000.0)*Clock.getSimulationSpeed());
        }
        Date tmpTime = new Date();
        return Math.round(((tmpTime.getTime()-start.getTime())/1000.0)*Clock.getSimulationSpeed());
    }

    public void setTimeEntered(String date){
        timeEntered = date;
    }

    public void setTimeLeft(String date){
        timeLeft = date;
        this.end = new Date();
    }

    public boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(boolean b) {
        this.isPaid = b;
    }

}
