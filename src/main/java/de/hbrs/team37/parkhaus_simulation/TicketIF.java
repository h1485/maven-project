package de.hbrs.team37.parkhaus_simulation;

import java.util.Date;

public interface TicketIF {
    String getHashcode();
    Date getStart();
    long getTime();
    void setTimeEntered(String date);
    void setTimeLeft(String date);
    boolean getIsPaid();
    void setIsPaid(boolean b);
}
