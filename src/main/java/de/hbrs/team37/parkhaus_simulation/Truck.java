package de.hbrs.team37.parkhaus_simulation;

public class Truck extends Vehicle{
    public Truck(String carID, String customerType){
        super(carID, customerType, MultitonVehicleTypes.getTruckInstance().getBasePrice()/60);
    }

    @Override
    public String toString(){
       return MultitonVehicleTypes.getTruckInstance().getName() + "," + super.toString();
    }
}
