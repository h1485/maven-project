package de.hbrs.team37.parkhaus_simulation;

import java.util.Date;

public abstract class Vehicle implements VehicleIF {
    private String customerType;
    private final Ticket ticket;
    private final String carID;
    private final double price;

    public Vehicle(String carID, String customer, double price){
        this.carID = carID;
        ticket = new Ticket(carID);
        MultitonCustomerTypes foundCustomerType = MultitonCustomerTypes.getInstance(customer);
        switch (customer) {
            case "Family":
            case "Business":
                this.customerType = foundCustomerType.getName();
                this.price = price * foundCustomerType.getDiscount();
                break;
            default:
                this.customerType = MultitonCustomerTypes.getStandardInstance().getName();
                this.price = price * MultitonCustomerTypes.getStandardInstance().getDiscount();
        }
    }

    public String getCarID() {
        return carID;
    }

    public String getCustomerType(){
        return customerType;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public double getPrice() {
        return HelperClass.round((ticket.getTime() * price), 2);
    }

    public double getRawPrice() {
        return price;
    }

    @Override
    public String toString() {
        return carID + "," + customerType + ","
                + getTicket().getHashcode().substring(10,20).toUpperCase() + "," + getPrice() + ","
                + HelperClass.normalizeDuration(getTicket().getTime()) + ","
                + getTicket().timeEntered + ","
                + getTicket().timeLeft;
    }

    public String storeFormat(){
        long end = 0;
        if(getTicket().end == null)
            end = new Date().getTime();
        else
            end = getTicket().end.getTime();
        return this + "," +getTicket().start.getTime() + "," + end;
    }

    public void setCustomerType(String customer){
        this.customerType = customer;
    }

    public void setEnterDate(String date){
        this.ticket.setTimeEntered(date);
    }

    public void setTimeLeft(String date){
        this.ticket.setTimeLeft(date);
    }
}

