package de.hbrs.team37.parkhaus_simulation;

 interface VehicleIF {
     Ticket getTicket();
     String getCarID();
     String getCustomerType();
     double getPrice();
     double getRawPrice();
     String toString();
     String storeFormat();
}
