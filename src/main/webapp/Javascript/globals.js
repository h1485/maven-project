let servers =
    [

        "http://localhost:8080/team_37_parkhaus_war_exploded/Car",
        "http://localhost:8080/team_37_parkhaus_war_exploded/Truck",
        "http://localhost:8080/team_37_parkhaus_war_exploded/Bike"

        // "http://sepp-test.inf.h-brs.de:8080/team_37_parkhaus-1.0/Car",
        // "http://sepp-test.inf.h-brs.de:8080/team_37_parkhaus-1.0/Truck",
        // "http://sepp-test.inf.h-brs.de:8080/team_37_parkhaus-1.0/Bike"
    ]

// [[capacity,carType,availability],[...]]
let serverConfigs = [];
console.log(serverConfigs); //SonarQube thinks that this array isn't used.

// 3-dimensional array [[[position,carType,id,customer_type,ticketHash,time,price],[...],[]] , [[],[],[]]]
let serverCars = [];

//cars that want to leave and didnt pay yet
let leavingCars = [];
console.log(leavingCars); //SonarQube thinks that this array isn't used.

let currentTime = "";
const days = ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"];



async function fetchLeavingCars() {
    await fetch(servers[0] + "?cmd=leavingCars")
        .then(res => res.text())
        .then(data => {
            for(const car of data.split("|"))
                leavingCars.push(car.split(","));
        })
}

//fetch config from the list of servlets
async function fetchConfig() {
    for(const url of servers) {
        await fetch(url + "?cmd=config")
            .then(res => res.text())
            .then(data => {
                serverConfigs.push(data.split(','));
            })
    }
}

async function fetchCurrentTime(){
    await fetch(servers[0] + "?cmd=currentTime")
        .then(res => res.text())
        .then(responseData => {
            currentTime = responseData;
        })
}
