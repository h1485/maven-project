document.addEventListener("DOMContentLoaded", async ()=> {
    await fetchLeavingCars();
    const payButton = document.querySelector(".payButton");
    const barrier = document.querySelector('.barrier');
    const auto = document.querySelector('.auto');
    const payment = document.querySelector(".payment");
    const ticketInput = document.querySelector('.ticketNr');
    const amount = document.querySelector(".amount");
    const greenLight = document.querySelector(".greenLight");
    const yellowLight = document.querySelector(".yellowLight");
    const redLight = document.querySelector(".redLight");

    ticketInput.addEventListener("input", (e)=> {
        e.currentTarget.value = e.currentTarget.value.replace(/\s/g, '');
        let ticketNr = e.currentTarget.value;
        auto.style.backgroundImage = "none";
        if(ticketNr.length > 0){
            amount.value = "404 ticket not found";
        } else {
            amount.value = "";
        }
        let vehicleType;
        let customerType;
        for(let carIndex=0; carIndex < leavingCars.length; carIndex++){
            if (ticketNr === leavingCars[carIndex][3] ||
            ticketNr === "#" + leavingCars[carIndex][3]) {
                price = parseFloat(leavingCars[carIndex][4]);
                vehicleType = leavingCars[carIndex][0];
                customerType = leavingCars[carIndex][2];
                amount.value = price + " €";

                if (vehicleType === "Car"){
                    if (customerType === "Standard") auto.style.backgroundImage = 'url("./icons/carStandard.svg")';
                    else if (customerType === "Family") auto.style.backgroundImage = 'url("./icons/carFamily.svg")';
                    else auto.style.backgroundImage = 'url("./icons/carBusiness.svg")';
                }else if (vehicleType === "Truck") {
                    if (customerType === "Standard") auto.style.backgroundImage = 'url("./icons/truckStandard.svg")';
                    else auto.style.backgroundImage = 'url("./icons/truckBusiness.svg")';
                }else {
                    if (customerType === "Standard") auto.style.backgroundImage = 'url("./icons/motorcycleStandard.svg")';
                    else auto.style.backgroundImage = 'url("./icons/motorcycleBusiness.svg")';
                }

                break;
            }
        }
    })

    payButton.addEventListener("click", async ()=> {
        await payTicket(ticketInput.value);
        removeTicket(ticketInput.value);
        payButton.disabled = true;
        payment.value = "";
        ticketInput.value = "";
        amount.value = "";
        price = -1;
        animateExit();
    })

    payment.addEventListener("input" ,(e)=> {
        try {
            let betrag = parseFloat(e.currentTarget.value);
            payButton.disabled = price === -1 || betrag < price || isNaN(betrag);
        }catch (exception) {
            payButton.disabled = true;
            console.log(exception);
        }
    })



    function animateExit() {
        barrier.classList.add("active");
        auto.classList.add("active");

        setTimeout(()=> {
            barrier.classList.remove("active");
            auto.classList.remove("active");
            auto.style.backgroundImage = "none";
        },4000)

        // disable red enable yellow
        redLight.classList.toggle("active");
        yellowLight.classList.toggle("active");

        //enable green disable yellow
        setTimeout(()=> {
            greenLight.classList.toggle("active");
            yellowLight.classList.toggle("active");
        },1000)
        // reset lights to initial state
        setTimeout(()=> {
            greenLight.classList.toggle("active");
            redLight.classList.toggle("active");
        },3000)
    }

})

let price = -1;


async function payTicket(ticketNr = "") {
    if(ticketNr.charAt(0) === '#'){
        ticketNr = ticketNr.substring(1);
    }

    let data = "payTicket," + ticketNr;
    for(const url of servers){
        await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'text/html'
            },
            body: data,
        })
    }
}


//only local
function removeTicket(ticketNr){
    //remove the '#' at the start
    if(ticketNr.charAt(0) === '#'){
        ticketNr = ticketNr.substring(1);
    }

    for (let carIndex = 0; carIndex < leavingCars.length; carIndex++){
        if(leavingCars[carIndex].includes(ticketNr)) {
            leavingCars.splice(carIndex, 1);
            break;
        }
    }

}

