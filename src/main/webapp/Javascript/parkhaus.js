document.addEventListener("DOMContentLoaded", async ()=> {
    await fetchCurrentTime();
    await createParkhausFromConfig();
    await displayCurrentServerTime();
    createModal("");
    await addNotPaidTickets();
    await addExistingCars();
    await addButtonsOnclickEvents();
})

//saving the number of cars for every floor
let numberCars = [0,0,0];

async function displayCurrentServerTime() {
    const serverTime = document.querySelector(".serverTime");
    let hrs = 0;
    let minutes = 0;

    let dayIndex = days.indexOf(currentTime.split(" ")[0]);
    hrs = currentTime.split(" ")[1].split(":")[0];
    minutes = currentTime.split(" ")[1].split(":")[1];
    serverTime.innerHTML = days[dayIndex] + "  " + hrs + ":" + minutes;
    setInterval(()=> {
        minutes++;

        if(minutes % 60 === 0){
            minutes = 0;
            hrs++;
        }

        if(hrs % 24 === 0){
            if(hrs !== 0)
                dayIndex++;
            hrs = 0;
        }

        if(minutes < 10) minutes = "0" + minutes;
        
        if(hrs < 10) {
            serverTime.innerHTML = days[dayIndex % 7] + "  0" + hrs + ":" + minutes;
        }else {
            serverTime.innerHTML = days[dayIndex % 7] + "  " + hrs + ":" + minutes;
        }

    },1000 / parseFloat(serverConfigs[0][3]))

}



//fetch cars from the list of servlets
async function fetchCars() {
    for (const url of servers) {
        await fetch(url + "?cmd=cars")
            .then(res => res.text())
            .then(data => {
                let carsAsString = data.split('|');
                let cars = []
                for (const stringCar of carsAsString){
                    cars.push(stringCar.split(','))
                    console.log(cars);
                }
                serverCars.push(cars)
            })
    }
}

function createModal(message){
    const modal = document.createElement("div");
    modal.classList.add("modal");
    modal.onclick = function(){modal.style.display = "none"};

    const modalContent = document.createElement("div");
    modalContent.classList.add("modalContent");

    const modalHeader = document.createElement("div");
    modalHeader.classList.add("modalHeader");
    const closeButton = document.createElement("span");
    closeButton.innerHTML = "close";
    closeButton.classList.add("modalClose");
    modalHeader.appendChild(closeButton);
    const title = document.createElement("h3");
    title.innerHTML = "Info!";
    modalHeader.appendChild(title);

    const modalBody = document.createElement("div");
    modalBody.classList.add("modalBody");
    const displayMessage = document.createElement("p");
    displayMessage.innerText = message;
    displayMessage.classList.add("modalMessage");
    modalBody.appendChild(displayMessage);

    const modalFooter = document.createElement("div");
    modalFooter.classList.add("modalFooter");

    modal.appendChild(modalContent);
    modalContent.appendChild(modalHeader);
    modalContent.appendChild(modalBody);
    modalContent.appendChild(modalFooter);

    const icon = document.createElement("div");
        icon.classList.add("alert");
        modalBody.appendChild(icon);
    document.body.appendChild(modal);
}

function popup(message){
        document.getElementsByClassName("modalMessage")[0].innerText = message;
        document.getElementsByClassName("modal")[0].style.display = "block";
}

//generate parking components for every servlet
async function createParkhausFromConfig() {
    await fetchConfig();
    serverConfigs.forEach((config, index) => {
        const etageContainer = createEtage(index, config[1]);
        const carPark = createCarpark(index);
        const buttonContainer = generateButtons(index);
        const buttonContainer2 = generateCapacityButtons(index);
        const selectCustomerType = createSelectCustomerType(index);


        for (let i = 0; i < config[0]; i++) {
            const parkplatzContainer = createParkplatzContainer(i,index);
            const parkplatz = createParkplatz();
            const parkplatzNr = createParkplatzNr(i+1);
            parkplatzContainer.appendChild(parkplatz);
            parkplatzContainer.appendChild(parkplatzNr);

            carPark.appendChild(parkplatzContainer);
        }

        etageContainer.appendChild(selectCustomerType);
        etageContainer.appendChild(carPark);
        etageContainer.appendChild(buttonContainer);
        etageContainer.appendChild(buttonContainer2);

        document.body.querySelector(".carparkContainer").appendChild(etageContainer)

    })
}

//add existing cars to the parking
async function addExistingCars() {
    await fetchCars();
    serverCars.forEach((array,etage) => {
        const parkingSlots = document.querySelector(`.carpark${etage}`)
            .querySelectorAll('.parkplatzContainer');
        array.forEach((car)=> {
            let pos = car[0];
            if (pos){
                parkingSlots[pos].querySelector('.parkplatz').setAttribute("data-kundentyp", car[3]);
                numberCars[etage]++;
            }

        })
    })
}

async function addNotPaidTickets() {
    await fetchLeavingCars();
    const table = document.querySelector(".carTable table");
    for(const car of leavingCars) {
        if (car.length > 2) {
            let newCar = "X," + car;
            table.appendChild(createTableRow(newCar.split(",")));
        }
    }
}



// enter car in specific position
async function enter(pos, etage) {

    const customerType = document.getElementById(`select${etage}`).value;

    let data = `enter,${pos},${customerType}`;
    fetch(servers[etage], {
        method: 'POST',
        headers: {
            'Content-Type': 'text/html'
        },
        body: data,
    })
        .then(response => response.text())
        .then(responseData => {
            if(responseData === "notAvailable"){
                popup("This floor is not available at this moment. Please come back later!");
            }else{
                const parkingSlots = document.querySelector(`.carpark${etage}`)
                    .querySelectorAll('.parkplatzContainer');
                parkingSlots[pos].querySelector('.parkplatz').setAttribute("data-kundentyp", responseData);
                numberCars[etage]++;
            }
        })

}


//enter car randomly in specific floor (position as response from server)
async function randomEnter(etage) {
    let data = "randomEnter";
    fetch(servers[etage], {
            method: 'POST',
            headers: {
                'Content-Type': 'text/html'
            },
            body: data,
        })
        .then(response => response.text())
        .then(responseData => {
            if(responseData === "notAvailable"){
                popup("This floor is not available at this moment. Please come back later!");
            }else {
                let pos = parseInt(responseData.split(",")[0]);

                if(pos === -1){
                    popup("Etage ist voll");
                }else{
                    let kundentyp = responseData.split(",")[1]
                    const parkingSlots = document.querySelector(`.carpark${etage}`)
                        .querySelectorAll('.parkplatzContainer');
                    parkingSlots[pos].querySelector('.parkplatz').setAttribute("data-kundentyp", kundentyp);
                }
                numberCars[etage]++;
            }
        })
}

//clear specific position
async function leave(pos, etage) {
    let data= `leave,${pos}`;
    fetch(servers[etage], {
        method: 'POST',
        headers: {
            'Content-Type': 'text/html'
        },
        body: data,
    })
        .then(response => response.text())
        .then(responseData => {
            const parkingSlots = document.querySelector(`.carpark${etage}`)
                .querySelectorAll('.parkplatzContainer');
            parkingSlots[pos].querySelector('.parkplatz').setAttribute("data-kundentyp" , "");
            numberCars[etage]--;
            const table = document.querySelector(".carTable table");
            table.appendChild(createTableRow(responseData.split(",")));
        })
}

//randomly clear a position in a specific floor (cleared position as response from server)
async function randomLeave(etage) {
    let data = "randomLeave";
    fetch(servers[etage], {
        method: 'POST',
        headers: {
            'Content-Type': 'text/html'
        },
        body: data,
    })
        .then(response => response.text())
        .then(responseData => {
            let pos = parseInt(responseData.split(",")[0]);
            if (pos === -1) {
                popup("Etage ist leer");
            }else {
                const parkingSlots = document.querySelector(`.carpark${etage}`)
                    .querySelectorAll('.parkplatzContainer')
                parkingSlots[pos].querySelector('.parkplatz').setAttribute("data-kundentyp" , "");
                numberCars[etage]--;
                const table = document.querySelector(".carTable table");
                const tables = createTableRow(responseData.split(","));
                table.appendChild(tables);
            }
        })
}

async function resetFloor(etage) {
    let data = "reset";
    await fetch(servers[etage], {
        method: 'POST',
        headers: {
            'Content-Type': 'text/html'
        },
        body: data,
    })

    const parkingSlots = document.querySelector(`.carpark${etage}`)
        .querySelectorAll('.parkplatz')
    parkingSlots.forEach(slot => {
        slot.setAttribute("data-kundentyp", "");
    })
    numberCars[etage] = 0;

}

//create random enter button
function createRandomEnterButton(etage) {
    const randomEnterButton = document.createElement('button');
    randomEnterButton.classList.add("randomEnter");
    randomEnterButton.setAttribute('data-etage', `${etage}`);
    randomEnterButton.innerHTML = "ENTER";

    randomEnterButton.addEventListener('click', async (e)=> {
        await randomEnter(e.currentTarget.getAttribute('data-etage'));
    })

    return randomEnterButton;
}

//create random leave button
function createRandomLeaveButton(etage) {
    const randomLeaveButton = document.createElement('button');
    randomLeaveButton.classList.add("randomLeave");
    randomLeaveButton.setAttribute('data-etage', `${etage}`);
    randomLeaveButton.innerHTML = "LEAVE";

    randomLeaveButton.addEventListener('click', async (e)=> {
        await randomLeave(e.currentTarget.getAttribute('data-etage'));
    })


    return randomLeaveButton;
}

function createResetButton(etage){
    const resetButton = document.createElement("button");
    resetButton.classList.add("reset");
    resetButton.setAttribute('data-etage', `${etage}`)
    resetButton.innerHTML = "RESET";

    resetButton.addEventListener('click', async (e)=> {
        await resetFloor(e.currentTarget.getAttribute('data-etage'));
    })

    return resetButton;
}

//floor container
function createEtage(index, carType = "") {
    const etageContainer = document.createElement("div");
    etageContainer.classList.add("etage");
    etageContainer.setAttribute("data-type", carType)

    const title = document.createElement("h3");
    if(index === 0) {
        title.innerHTML = "Erdgeschoss - " + carType;
    }else {
        title.innerHTML = index + ". Etage - " + carType;
    }
    etageContainer.appendChild(title);
    return etageContainer;
}

//create parking (container for positions)
function createCarpark(index) {
    const carpark = document.createElement('div');
    carpark.classList.add("carpark",`carpark${index}`);
    return carpark;
}

// parkplatz + nummer wrapper
function createParkplatzContainer(index, etage) {

    const parkplatzContainer = document.createElement('div');
    parkplatzContainer.classList.add('parkplatzContainer');
    parkplatzContainer.setAttribute('data-number', `${index}`);
    parkplatzContainer.setAttribute('data-etage', `${etage}`);

    parkplatzContainer.addEventListener(("click"), async (e) => {
            if(e.currentTarget.querySelector('.parkplatz').getAttribute("data-kundentyp").length > 0){
                await leave(e.currentTarget.getAttribute('data-number'), etage)

            }
            else{
                await enter(e.currentTarget.getAttribute('data-number'),etage)

            }
    })

    return parkplatzContainer;
}

//parkplatz
function createParkplatz(kundentyp = "") {
    const parkplatz = document.createElement('div');
    parkplatz.classList.add('parkplatz');
    parkplatz.setAttribute("data-kundentyp", kundentyp);
    return parkplatz;
}

//container for the number
function createParkplatzNr(i) {
    const parkplatzNr = document.createElement('div');
    parkplatzNr.innerHTML = i;

    return parkplatzNr;
}

function generateButtons(i) {
    const buttonContainer = document.createElement("div");
    buttonContainer.classList.add("buttonContainer");
    buttonContainer.appendChild(createRandomEnterButton(i));
    buttonContainer.appendChild(createRandomLeaveButton(i));
    buttonContainer.appendChild(createResetButton(i));
    buttonContainer.appendChild(createSimulationButton(i));

    return buttonContainer;
}

function generateCapacityButtons(index) {
    const buttonContainer = document.createElement("div");
    buttonContainer.classList.add("buttonContainer");
    buttonContainer.appendChild(createDecreaseButton(index));
    buttonContainer.appendChild(createIncreaseButton(index));

    return buttonContainer;
}

function createIncreaseButton(etage) {
    const incButton = document.createElement("button");
    incButton.setAttribute("data-etage", `${etage}`);
    incButton.innerHTML = "+1";
    incButton.classList.add("increaseButton");

    incButton.addEventListener("click", async () => {
        let data = "increase";
        fetch(servers[etage], {
            method: 'POST',
            headers: {
                'Content-Type': 'text/html'
            },
            body: data,
        })
            .then(response => response.text())
            .then(() => {
                serverConfigs[etage][0]++;
                const e = document.querySelector(`.carpark${etage}`);
                const parkplatzcontainer = createParkplatzContainer(parseInt(serverConfigs[etage][0]) - 1, etage);
                parkplatzcontainer.appendChild(createParkplatz(""));
                parkplatzcontainer.appendChild(createParkplatzNr(parseInt(serverConfigs[etage][0])));
                e.appendChild(parkplatzcontainer);
            })
    })

    return incButton;
}

function createDecreaseButton(etage) {
    const decButton = document.createElement("button");
    decButton.setAttribute("data-etage", `${etage}`);
    decButton.innerHTML = "-1";
    decButton.classList.add("decreaseButton");

    decButton.addEventListener("click", async () => {
        console.log("config: " + serverConfigs[etage][0]);
        let data = "decrease";
        await fetch(servers[etage], {
            method: 'POST',
            headers: {
                'Content-Type': 'text/html'
            },
            body: data,
        })
            .then(response => response.text())
            .then(responseData => {
                if(parseInt(responseData) < parseInt(serverConfigs[etage][0])){
                    console.log( "data: "+ responseData)
                    serverConfigs[etage][0]--;
                    const e = document.querySelector(`.carpark${etage}`);
                    e.removeChild(e.lastElementChild);
                }else{
                    console.log(responseData);
                    popup("Letzer Parkplatz ist noch besetzt!");
                }

            })
    })

    return decButton;
}

function createSimulationButton(etage) {
    const simButton = document.createElement("button");
    simButton.classList.add("simulationButton");
    simButton.setAttribute("data-simulation","0");
    simButton.setAttribute("data-etage",`${etage}`);
    simButton.innerHTML = "Start Simulation";


    let interval;
    simButton.addEventListener("click",async (e)=> {
        if( parseInt(e.currentTarget.getAttribute("data-simulation")) === 0) {
            interval = await simulate(etage);
            e.currentTarget.setAttribute("data-simulation", "1");
            simButton.innerHTML = "Stop Simulation"
        }else {
            e.currentTarget.setAttribute("data-simulation", "0");
            simButton.innerHTML = "Start Simulation";
            clearInterval(interval);
        }
    })

    return simButton;
}

async function simulate(etage) {
    const capacity = serverConfigs[etage][0];
    let timeout = 1000 / parseFloat(serverConfigs[0][3]);
    if(timeout > 1000 ) {
        timeout = 3000;
    }


    return setInterval(async ()=> {
        const crypto = window.crypto;
        let array = new Uint32Array(1);
        let random = Math.floor(crypto.getRandomValues(array) * 2);
        let number = numberCars[etage];
        if( (number / capacity) < 0.3) {
            await randomEnter(etage)
        }else if((number / capacity) > 0.7){
            await randomLeave(etage)
        }else {
            if(random === 0) await randomEnter(etage)
            else await randomLeave(etage);
        }

    },timeout )

}

function createTableRow(data = [""]) {
    const row = document.createElement("tr");
    row.classList.add(data[3]);

    // const etageNr = document.createElement("td");
    // etageNr.innerHTML = etage;
    //
    // const parkplatzNr = document.createElement("td");
    // parkplatzNr.innerHTML = data[0];

    const autotyp = document.createElement("td");
    autotyp.innerHTML = data[1];

    const autoID = document.createElement("td");
    autoID.innerHTML = data[2];

    const kundenTyp = document.createElement("td");
    kundenTyp.innerHTML = data[3];

    const ticketNr = document.createElement("td");
    ticketNr.innerHTML = "#" + data[4];

    const price = document.createElement("td")
    price.innerHTML = data[5] + " €";

    const durationTime = document.createElement("td");
    durationTime.innerHTML = data[6];

    const fromTime = document.createElement("td");
    fromTime.innerHTML = data[7];

    const toTime = document.createElement("td");
    toTime.innerHTML = data[8];


    // row.appendChild(etageNr);
    // row.appendChild(parkplatzNr);
    row.appendChild(autotyp);
    row.appendChild(autoID);
    row.appendChild(kundenTyp);
    row.appendChild(ticketNr);
    row.appendChild(fromTime);
    row.appendChild(toTime);
    row.appendChild(durationTime);
    row.appendChild(price);

    return row;

}


function addButtonsOnclickEvents() {
    let filters =  ["table tr.Standard", "table tr.Family", "table tr.Business"];
    let types = [".selectStandard", ".selectFamily", ".selectBusiness"]

    for(let i = 0; i < filters.length; i++){
        document.querySelector(types[i]).addEventListener("click", () => {
            //disable other filters
            const activeRows = document.querySelectorAll("table tr.active")
            if (activeRows) {
                activeRows.forEach(row => {
                    row.classList.remove("active");
                })
            }

            //highlight standard
            const rows = document.querySelectorAll(filters[i]);
            if (rows) {
                rows.forEach(row => {
                    row.classList.toggle("active");
                })
            }
        })
    }

    document.querySelector(".resetFilters").addEventListener("click", () => {
        //disable other filters
        const activeRows = document.querySelectorAll("table tr.active")
        if (activeRows) {
            activeRows.forEach(row => {
                row.classList.remove("active");
            })
        }

    })
}

function createSelectCustomerType(etage) {
    const selectContainer = document.createElement("div");
    selectContainer.classList.add("selectContainer");

    const select = document.createElement("select");
    select.id = `select${etage}`;
    select.name = "select";

    let customerTypes = ["Random","Standard", "Family", "Business"]

    for(const customer of customerTypes ) {
        const option = document.createElement("option");
        option.value = customer;
        option.innerHTML = customer;

        if(customer === "Family" && etage !== 0)
            continue;

        select.appendChild(option)

    }
    selectContainer.appendChild(select);

    return selectContainer;
}