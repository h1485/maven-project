const basePrices = [];
const discounts = [];

document.addEventListener("DOMContentLoaded", async ()=> {
    await fetchConfig();
    await fetchCurrentTime();
    await fetchPrices();
    //switch buttons
    const switchPKW = document.getElementById("switchPKW");
    const switchLKW = document.getElementById("switchLKW");
    const switchBike = document.getElementById("switchBike");

    switchPKW.checked = (serverConfigs[0][2] === "true");
    switchLKW.checked = (serverConfigs[1][2] === "true");
    switchBike.checked = (serverConfigs[2][2] === "true");

    // save previous state
    const oldAvailablePKW = switchPKW.checked ? "enable" : "disable";
    const oldAvailableLKW = switchLKW.checked ? "enable" : "disable";
    const oldAvailableBike = switchBike.checked ? "enable" : "disable";

    //baseprices
    const carPrice = document.getElementById("basePricePKW");
    carPrice.value = basePrices[0];

    const truckPrice = document.getElementById("basePriceLKW");
    truckPrice.value = basePrices[1];

    const bikePrice = document.getElementById("basePriceBike");
    bikePrice.value = basePrices[2];

    //discounts
    const discountStandard = document.getElementById("discountStandard");
    discountStandard.value = discounts[0];

    const discountFamily = document.getElementById("discountFamily");
    discountFamily.value = discounts[1];

    const discountBusiness = document.getElementById("discountBusiness");
    discountBusiness.value = discounts[2];

    // speed buttons
    const speedRadioButtons = document.querySelectorAll("input[name='simulationSpeed']");

    speedRadioButtons.forEach((btn)=> {
        if(btn.value === serverConfigs[0][3]) {
            btn.checked = true;
        }
    })
    // save old speed
    const oldSelectedSpeed = document.querySelector("input[name='simulationSpeed']:checked").value;


    // capacity buttons
    const capacityPKW = document.getElementById("capacityPKW");
    const capacityLKW = document.getElementById("capacityLKW");
    const capacityBike = document.getElementById("capacityBike");

    capacityPKW.value = serverConfigs[0][0];
    capacityLKW.value = serverConfigs[1][0];
    capacityBike.value = serverConfigs[2][0];

    const oldCapacityPKW = serverConfigs[0][0];
    const oldCapacityLKW = serverConfigs[1][0];
    const oldCapacityBike = serverConfigs[2][0];


    // select days
    const selectDay = document.getElementById("selectDay");
    selectDay.value = days.indexOf(currentTime.split(" ")[0]);

    const oldSelectedDay = selectDay.value;



    const saveButton = document.querySelector(".saveButton");
    const revertButton = document.querySelector(".revertButton");
    saveButton.addEventListener("click", async ()=> {
        const availablePKW = switchPKW.checked ? "enable" : "disable";
        const availableLKW = switchLKW.checked ? "enable" : "disable";
        const availableBike = switchBike.checked ? "enable" : "disable";

        const selectedSpeed = document.querySelector("input[name='simulationSpeed']:checked").value;

        enableDisableFloors([availablePKW,availableLKW,availableBike]);
        await changeSimulationSpeed(selectedSpeed);
        changeCapacity([capacityPKW.value,capacityLKW.value, capacityBike.value]);
        await changeDay(selectDay.value);
        await changeBasePrices([carPrice.value, truckPrice.value, bikePrice.value]);
        await changeDiscounts([discountStandard.value, discountFamily.value, discountBusiness.value])

        saveButton.disabled = true;
        revertButton.disabled = false;
        setTimeout(()=> {
            saveButton.disabled = false;
        }, 3000)
    })



    revertButton.addEventListener("click",async ()=> {
        enableDisableFloors([oldAvailablePKW,oldAvailableLKW,oldAvailableBike]);
        await changeSimulationSpeed(oldSelectedSpeed);
        changeCapacity([oldCapacityPKW,oldCapacityLKW, oldCapacityBike]);
        await changeDay(oldSelectedDay);
        await changeBasePrices(basePrices);
        await changeDiscounts(discounts);

        revertButton.disabled = true;
        location.reload();
    })

    const resetAllButton = document.querySelector(".resetAll");
    resetAllButton.addEventListener("click",async ()=> {
        await resetAll();
    })

})

function enableDisableFloors(availabilities) {
    availabilities.forEach(async (availabilitiy, index) => {
        await fetch(servers[index], {
            method: 'POST',
            headers: {
                'Content-Type': 'text/html'
            },
            body: availabilitiy,
        })
    })
}

async function changeSimulationSpeed(speed = 1.0) {
    const data = "simulationSpeed," + speed
    await fetch(servers[0], {
        method: 'POST',
        headers: {
            'Content-Type': 'text/html'
        },
        body: data,
    })
}

function changeCapacity(floors = []) {
    floors.forEach(async (floor, index) => {
        let data = "changeCapacity," + floor;
        await fetch(servers[index], {
            method: 'POST',
            headers: {
                'Content-Type': 'text/html'
            },
            body: data,
        })
    })
}

async function changeDay(day) {
    let data = "clockDay," + day;
    for (const url of servers){
        await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'text/html'
            },
            body: data,
        })
    }
}

async function resetAll() {
    let data = "resetAll";
    for (const url of servers){
        await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'text/html'
            },
            body: data,
        })
    }
    location.reload();
}

// the new form of (.then)
async function fetchPrices() {
    const responseString = await fetch(servers[0] + "?cmd=basePrices");
    const responseData = await responseString.text();
    for (const data of responseData.split('|')) {
        basePrices.push(parseFloat(data));
    }

    const responseString2 = await fetch(servers[0] + "?cmd=discounts");
    const responseData2 = await responseString2.text();
    for (const data of responseData2.split('|')) {
        discounts.push(parseFloat(data));
    }
}

async function changeBasePrices(prices = [2.0, 15.0, 1.0]) {
    const data = "changeBasePrice,"
    servers.forEach((server, index) => {
        fetch(server, {
            method: 'POST',
            headers: {
                'Content-Type': 'text/html'
            },
            body: data + prices[index],
        })
    })
}

async function changeDiscounts(priceFactors = [1.0, 0.7, 0.3]) {
    const data = "changeDiscounts," + priceFactors[0] + "/" + priceFactors[1] + "/" + priceFactors[2];
    await fetch(servers[0], {
        method: 'POST',
        headers: {
            'Content-Type': 'text/html'
        },
        body: data,
    })

}