let earningsCustomer;
let earningsCartype;
let earningsEntered;
let earningsLeft;
let myEarningsChart;

let numberCustomer;
let numberCartype;
let numberEntered;
let numberLeft;
let myNumberChart;


let sumCars;
let sumTrucks;
let sumBikes;

let sumLeavingVehicles;
let sumCollected;

document.addEventListener("DOMContentLoaded", async ()=>{
    await fetchEarnings();
    await fetchNumbers();
    await fetchSum();

    const tdSumCars = document.getElementById("sumCars");
    const tdSumTrucks = document.getElementById("sumTrucks");
    const tdSumBikes = document.getElementById("sumBikes");
    const tdSumLeaving = document.getElementById("sumLeavingVehicles");
    const tdSumCollected = document.getElementById("sumCollected");

    tdSumCars.innerHTML = parseFloat(sumCars).toFixed(2) + " €";
    tdSumTrucks.innerHTML = parseFloat(sumTrucks).toFixed(2) + " €";
    tdSumBikes.innerHTML = parseFloat(sumBikes).toFixed(2) + " €";
    tdSumLeaving.innerHTML = parseFloat(sumLeavingVehicles).toFixed(2) + " €";
    tdSumCollected.innerHTML = parseFloat(sumCollected).toFixed(2) + " €";


    const selectTypeEarnings = document.getElementById("selectTypeEarnings");
    const charTypeEarnings = document.getElementById("chartTypeEarnings");

    const selectTypeNumber = document.getElementById("selectTypeNumber");
    const charTypeNumber = document.getElementById("chartTypeNumber");


    selectTypeEarnings.addEventListener("change",()=> {
        switch (selectTypeEarnings.value){
            case "customer":
                changeEarningsChart( charTypeEarnings.value, earningsCustomer);
                break;
            case "cartype":
                changeEarningsChart( charTypeEarnings.value, earningsCartype);
                break;
            case "time_entered":
                changeEarningsChart(charTypeEarnings.value, earningsEntered);
                break;
            case "time_left":
                changeEarningsChart(charTypeEarnings.value, earningsLeft);
                break;
            default:
                changeEarningsChart( charTypeEarnings.value, earningsCartype);
        }
    })

    charTypeEarnings.addEventListener("change",()=> {
        switch (selectTypeEarnings.value){
            case "customer":
                changeEarningsChart( charTypeEarnings.value, earningsCustomer);
                break;
            case "cartype":
                changeEarningsChart( charTypeEarnings.value, earningsCartype);
                break;
            case "time_entered":
                changeEarningsChart(charTypeEarnings.value, earningsEntered);
                break;
            case "time_left":
                changeEarningsChart(charTypeEarnings.value, earningsLeft);
                break;
            default:
                changeEarningsChart( charTypeEarnings.value, earningsCartype);
        }
    })


    selectTypeNumber.addEventListener("change",()=> {
        switch (selectTypeNumber.value){
            case "customer":
                changeNumberChart( charTypeNumber.value, numberCustomer);
                break;
            case "cartype":
                changeNumberChart( charTypeNumber.value, numberCartype);
                break;
            case "time_entered":
                changeNumberChart(charTypeNumber.value, numberEntered);
                break;
            case "time_left":
                changeNumberChart(charTypeNumber.value, numberLeft);
                break;
            default:
                changeNumberChart( charTypeNumber.value, numberCustomer);
        }
    })


    charTypeNumber.addEventListener("change",()=> {
        switch (selectTypeNumber.value){
            case "customer":
                changeNumberChart( charTypeNumber.value, numberCustomer);
                break;
            case "cartype":
                changeNumberChart( charTypeNumber.value, numberCartype);
                break;
            case "time_entered":
                changeNumberChart(charTypeNumber.value, numberEntered);
                break;
            case "time_left":
                changeNumberChart(charTypeNumber.value, numberLeft);
                break;
            default:
                changeNumberChart( charTypeNumber.value, numberCustomer);
        }
    })




    function changeEarningsChart(type, data) {
        const earningsChart = document.getElementById("earningsChart").getContext("2d");
        myEarningsChart.destroy();
        myEarningsChart = new Chart(earningsChart, {type:type, data: data});
    }

    function changeNumberChart(type, data) {
        const numberChart = document.getElementById("numbersChart").getContext("2d");
        myNumberChart.destroy();
        myNumberChart = new Chart(numberChart, {type:type, data: data});
    }


})

async function fetchEarnings() {
    fetch(servers[0]+"?cmd=chartEarnings")
        .then(res => res.text())
        .then(responseData => {
            let data = responseData.split("||");
            let customerData = data[0];
            let cartypeData = data[1];
            let enteredData = data[2];
            let leftData = data[3];
            earningsCustomer = JSON.parse(customerData.split("|")[1]);
            earningsCartype = JSON.parse(cartypeData.split("|")[1]);
            earningsEntered = JSON.parse(enteredData.split("|")[1]);
            console.log(enteredData);
            earningsLeft = JSON.parse(leftData.split("|")[1]);
            console.log(earningsLeft);

            const earningsChart = document.getElementById("earningsChart").getContext("2d");
            myEarningsChart = new Chart(earningsChart, {type:'pie', data: earningsCustomer});


        })
}


async function fetchNumbers() {
    fetch(servers[0]+"?cmd=chartNumbers")
        .then(res => res.text())
        .then(responseData => {
            let customerData = responseData.split("||")[0];
            let cartypeData = responseData.split("||")[1];
            let enteredData = responseData.split("||")[2];
            let leftData = responseData.split("||")[3];
            numberCustomer = JSON.parse(customerData.split("|")[1]);
            numberCartype = JSON.parse(cartypeData.split("|")[1]);
            numberEntered = JSON.parse(enteredData.split("|")[1]);
            numberLeft = JSON.parse(leftData.split("|")[1]);

            const numberChart = document.getElementById("numbersChart").getContext("2d");
            myNumberChart = new Chart(numberChart, {type:'pie', data: numberCustomer});
        })
}

async function fetchSum() {
    await fetch(servers[0]+"?cmd=getCurrentSum")
        .then(res => res.text())
        .then(responseData => {
            sumCars = responseData;
        })

    await fetch(servers[1]+"?cmd=getCurrentSum")
        .then(res => res.text())
        .then(responseData => {
            sumTrucks = responseData;
        })

    await fetch(servers[2]+"?cmd=getCurrentSum")
        .then(res => res.text())
        .then(responseData => {
            sumBikes = responseData;
        })

    await fetch(servers[2]+"?cmd=getStoredSum")
        .then(res => res.text())
        .then(responseData => {
            sumLeavingVehicles = responseData;
        })

    sumCollected = parseFloat(sumCars) + parseFloat(sumTrucks) + parseFloat(sumBikes) + parseFloat(sumLeavingVehicles);
}





