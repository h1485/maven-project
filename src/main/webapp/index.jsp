<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="de" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta name="author" content="Team89 (C) 2021">
    <meta name="description" content="Software Engineering 1 (SE1)">
    <meta name="license" content="The MIT License (MIT)">
    <meta name="theme-color" content="#239BD1"/>
    <meta property="og:title" content="Software Engineering 1 Project(SE1)">
    <meta property="og:description" content="Bachelor Course Software Engineering 1 (SE1), Hochschule Bonn-Rhein-Sieg.">
    <link rel="stylesheet" href= "styles/style.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Orbitron:wght@500&display=swap" rel="stylesheet">
    <title>Parkhaus [Team 37]</title>
    <script src="Javascript/globals.js"></script>
    <script src='Javascript/parkhaus.js'></script>
    <script src='Javascript/slider.js'></script>

</head>
<body>
    <header>
        <button class="nextFloor">&#8594;</button>
        <button class="previousFloor">&#8592;</button>
        <nav class="pageLinks">
            <ul>
                <li class="active"><a href="index.jsp" id="linkParkhaus">Parking</a></li>
                <li><a href="kasse.jsp" id="linkCheckout">Checkout</a></li>
                <li><a href="statistics.jsp" id="linkStatistics">Statistics</a></li>
                <li><a href="settings.jsp" id="linkSettings">Settings</a></li>
            </ul>
        </nav>
        <img id="hbrs"
             src="icons/hbrs.png"
             alt="hbrs-logo"
        />
        <img id="garage" src="https://kaul.inf.h-brs.de/ccmjs/mkaul-components/parkhaus/resources/parking_garage.png"
             alt="Parkhaus-img"
        />
    </header>
    <div class="serverTime">

    </div>

    <section class="carparkContent">

        <div class="carparkContainer">

        </div>
        <div class="carTable">
            <div class="buttonContainer">
                <button class="selectStandard">Select Standard</button>
                <button class="selectFamily">Select Family</button>
                <button class="selectBusiness">Select Business</button>
                <button class="resetFilters">Reset Filters</button>
            </div>

            <table>
                <caption><h3 class="title">Open Tickets (Not paid yet)</h3></caption>
                <tr>
                    <th id="autotypColumn">Autotyp</th>
                    <th id="idColumn">Auto ID</th>
                    <th id="kundentypColumn">Kundentyp</th>
                    <th id="ticketColumn">Ticket Nr</th>
                    <th id="timeIn">From</th>
                    <th id="timeOut">to</th>
                    <th id="duration">Duration</th>
                    <th id="priceColumn">Price</th>
                </tr>
            </table>

        </div>

    </section>

    <footer>
        <h3>Authors (Team 37)</h3>
        <div>Minh Truong: <a href="mailto:minh.truong@smail.inf.h-brs.de">minh.truong@smail.inf.h-brs.de</a></div>
        <div>Ameur Khemissi: <a href="mailto:ameur.khemissi@smail.inf.h-brs.de">ameur.khemissi@smail.inf.h-brs.de</a></div>
    </footer>
</body>

</html>