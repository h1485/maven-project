<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="de" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <link rel="stylesheet" href="styles/style.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Libre+Barcode+128+Text&family=Source+Code+Pro:ital,wght@1,300&display=swap"
          rel="stylesheet">
    <script src="Javascript/globals.js"></script>
    <script src='Javascript/kasse.js' ></script>
    <title>Parkhaus Kasse</title>
</head>
<body>
    <header>
        <nav class="pageLinks">
            <ul>
                <li ><a href="index.jsp" id="linkParkhaus">Parking</a></li>
                <li class="active"><a href="kasse.jsp" id="linkCheckout">Checkout</a></li>
                <li><a href="statistics.jsp" id="linkStatistics">Statistics</a></li>
                <li><a href="settings.jsp" id="linkSettings">Settings</a></li>
            </ul>
        </nav>
        <img id="hbrs"
             src="icons/hbrs.png"
             alt="hbrs-logo"
        />
        <img id="garage" src="https://kaul.inf.h-brs.de/ccmjs/mkaul-components/parkhaus/resources/parking_garage.png"
             alt="Parkhaus-img"
        />
    </header>

    <div class="kasseContainer">
        <div class="kasse">
            <div>
                <label for="ticketNr">Ticket number:</label>
                <input type="text" class="ticketNr" name="ticketNr" id="ticketNr" placeholder="#ABCD1234EF">
            </div>
            <div class="amountToPay">
                <label for="amount">amount to pay: </label>
                <input type="text" class="amount" id="amount" readonly >
            </div>
            <div>
                <label for="payment">enter the price: </label>
                <input type="text" class="payment" id="payment" name="payment" placeholder="10.5 (€)">
            </div>
            <button class="payButton" disabled>Pay</button>
        </div>


        <div class="barrierContainer">
            <div class="barrier"></div>
            <div class="orangeBox">
                <div class="lightsContainer">
                    <div class="light greenLight"></div>
                    <div class="light yellowLight"></div>
                    <div class="light redLight active"></div>
                </div>
            </div>
            <div class="auto"></div>
        </div>
    </div>


    <footer>
        <h3>Authors (Team 37)</h3>
        <div>Minh Truong: <a href="mailto:minh.truong@smail.inf.h-brs.de">minh.truong@smail.inf.h-brs.de</a></div>
        <div>Ameur Khemissi: <a href="mailto:ameur.khemissi@smail.inf.h-brs.de">ameur.khemissi@smail.inf.h-brs.de</a></div>
    </footer>

</body>
</html>