<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>admin</title>
    <link rel="stylesheet" href="styles/style.css">
    <script src="Javascript/globals.js"></script>
    <script src='Javascript/settings.js'></script>
</head>
<body>

<header>
    <nav class="pageLinks">
        <ul>
            <li ><a href="index.jsp" id="linkParkhaus">Parking</a></li>
            <li><a href="kasse.jsp" id="linkCheckout">Checkout</a></li>
            <li><a href="statistics.jsp" id="linkStatistics">Statistics</a></li>
            <li class="active"><a href="settings.jsp" id="linkSettings">Settings</a></li>
        </ul>
    </nav>
    <img id="hbrs"
         src="icons/hbrs.png"
         alt="hbrs-logo"
    />
    <img id="garage" src="https://kaul.inf.h-brs.de/ccmjs/mkaul-components/parkhaus/resources/parking_garage.png"
         alt="Parkhaus-img"
    />
</header>

<table class="admin">
    <caption><h3 class="title">General Settings</h3></caption>
    <tr>
        <th scope="col">Day</th>
        <th scope="col">Simulation Speed</th>
    </tr>
    <tr>
        <td rowspan="3">
            <div  class="selectContainer">
                <label for="selectDay"></label>
                <select id="selectDay">
                    <option value="0">Monday</option>
                    <option value="1">Tuesday</option>
                    <option value="2">Wednesday</option>
                    <option value="3">Thursday</option>
                    <option value="4">Friday</option>
                    <option value="5">Saturday</option>
                    <option value="6">Sunday</option>
                </select>
            </div>
        </td>
        <td rowspan="3">
            <div class="simulationContainer">
                <div>
                    <input type="radio" id="realtime" name="simulationSpeed" value="1/60.0" checked>
                    <label for="realtime">Realtime Simulation</label>
                </div>
                <div>
                    <input type="radio" id="1s1min" name="simulationSpeed" value="1.0">
                    <label for="1s1min">1s (Realtime) = 1min (Servertime)</label>
                </div>
                <div>
                    <input type="radio" id="5xfaster" name="simulationSpeed" value="5.0">
                    <label for="5xfaster">5x faster</label>
                </div>
                <div>
                    <input type="radio" id="10xfaster" name="simulationSpeed" value="10.0">
                    <label for="10xfaster">10x faster</label>
                </div>
            </div>
        </td>
    </tr>

</table>
<div class="specificSettings">
    <table class="admin">
        <caption><h3 class="title">Floor Settings</h3></caption>
        <tr>
            <th scope="col">Floor</th>
            <th scope="col">Availability</th>
            <th scope="col">Base Price (€ / h)</th>
            <th scope="col">Capacity</th>
        </tr>
        <%--    erste Zeile --%>
        <tr>
            <td>Car Floor</td>
            <td>
                <label for="switchPKW"></label>
                <input type="checkbox" id="switchPKW" name="a">
            </td>
            <td>
                <label for="basePricePKW"></label>
                <input type="number" id="basePricePKW" name="basePricePKW" min="0">
            </td>
            <td>
                <label for="capacityPKW"></label>
                <input type="number" id="capacityPKW" min="0" max="100" step="5">
            </td>

        </tr>
        <%--zweite zeile--%>
        <tr>
            <td>Truck Floor</td>
            <td>
                <label for="switchLKW"></label>
                <input type="checkbox" id="switchLKW" name="a">
            </td>
            <td>
                <label for="basePriceLKW"></label>
                <input type="number" id="basePriceLKW" name="basePriceLKW" min="0">
            </td>
            <td>
                <label for="capacityLKW"></label>
                <input type="number" id="capacityLKW" min="0" max="100" step="5">
            </td>
        </tr>
        <%--dritte zeile--%>
        <tr>
            <td>Bike Floor</td>
            <td>
                <label for="switchBike"></label>
                <input type="checkbox" id="switchBike" name="a">
            </td>
            <td>
                <label for="basePriceBike"></label>
                <input type="number" id="basePriceBike" name="basePriceBike" min="0">
            </td>
            <td>
                <label for="capacityBike"></label>
                <input type="number" id="capacityBike" min="0" max="100" step="5">
            </td>
        </tr>

        <tr>

        </tr>
    </table>

    <table class="admin">
        <caption><h3 class="title">Subscriptions</h3></caption>
        <tr>
            <th scope="col">Subscription Type</th>
            <th scope="col">Price Factor</th>
        </tr>

        <tr>
            <td>Standard</td>
            <td>
                <label for="discountStandard"></label>
                <input type="number" id="discountStandard" min="0" max="1" step="0.1">
            </td>
        </tr>

        <tr>
            <td>Family</td>
            <td>
                <label for="discountFamily"></label>
                <input type="number" id="discountFamily" min="0" max="1" step="0.1">
            </td>
        </tr>

        <tr>
            <td>Business</td>
            <td>
                <label for="discountBusiness"></label>
                <input type="number" id="discountBusiness" min="0" max="1" step="0.1" >
            </td>
        </tr>

    </table>
</div>


<div>
    <button class="revertButton" disabled>Undo</button>
    <button class="saveButton">Save</button>
</div>
<div>
    <button class="resetAll">Reset Everything</button>
</div>

<footer>
    <h3>Authors (Team 37)</h3>
    <div>Minh Truong: <a href="mailto:minh.truong@smail.inf.h-brs.de">minh.truong@smail.inf.h-brs.de</a></div>
    <div>Ameur Khemissi: <a href="mailto:ameur.khemissi@smail.inf.h-brs.de">ameur.khemissi@smail.inf.h-brs.de</a></div>
</footer>

</body>
</html>
