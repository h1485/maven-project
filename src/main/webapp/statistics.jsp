<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="de">
<head>
    <title>Statistics</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.3.2/dist/chart.min.js"></script>
    <script src="Javascript/globals.js"></script>
    <script src='Javascript/statistics.js'></script>
    <link rel="stylesheet" href="styles/style.css"/>

</head>
<body>
<header>
    <nav class="pageLinks">
        <ul>
            <li><a href="index.jsp" id="linkParkhaus">Parking</a></li>
            <li><a href="kasse.jsp" id="linkCheckout">Checkout</a></li>
            <li class="active"><a href="statistics.jsp" id="linkStatistics">Statistics</a></li>
            <li><a href="settings.jsp" id="linkSettings">Settings</a></li>
        </ul>
    </nav>
    <img id="hbrs"
         src="icons/hbrs.png"
         alt="hbrs-logo"
    />
    <img id="garage" src="https://kaul.inf.h-brs.de/ccmjs/mkaul-components/parkhaus/resources/parking_garage.png"
         alt="Parkhaus-img"
    />
</header>

<div class="chartStats">
    <div class="wrapper">
        <h1>Earnings in €</h1>
        <div class="statSelect">
            <div class="selectContainer">
                <label for="selectTypeEarnings"></label>
                <select id="selectTypeEarnings" class="selection">
                    <option value="customer">Customer type</option>
                    <option value="cartype">Vehicle type</option>
                    <option value="time_entered">Days entered</option>
                    <option value="time_left">Days left</option>
                </select>
            </div>
            <div class="selectContainer">
                <label for="chartTypeEarnings"></label>
                <select id="chartTypeEarnings" class="selection">
                    <option value="pie">Pie Chart</option>
                    <option value="line">Line Chart</option>
                    <option value="bar">Bar Chart</option>
                    <option value="doughnut">Doughnut Chart</option>
                    <option value="polarArea">PolarArea Chart</option>
                    <option value="radar">Radar Chart</option>
                </select>
            </div>

        </div>

        <div class="chartContainer">
            <canvas id="earningsChart"></canvas>
        </div>
    </div>

    <div class="wrapper">
        <h1>Carpark History in numbers</h1>
        <div class="statSelect">
            <div  class="selectContainer">
                <label for="selectTypeNumber"></label>
                <select id="selectTypeNumber" class="selection">
                    <option value="customer">Customer type</option>
                    <option value="cartype">Vehicle type</option>
                    <option value="time_entered">Days entered</option>
                    <option value="time_left">Days left</option>
                </select>
            </div>

            <div  class="selectContainer">
                <label for="chartTypeNumber"></label>
                <select id="chartTypeNumber" class="selection">
                    <option value="pie">Pie Chart</option>
                    <option value="line">Line Chart</option>
                    <option value="bar">Bar Chart</option>
                    <option value="doughnut">Doughnut Chart</option>
                    <option value="polarArea">PolarArea Chart</option>
                    <option value="radar">Radar Chart</option>
                </select>
            </div>

        </div>

        <div class="chartContainer">
            <canvas id="numbersChart"></canvas>
        </div>
    </div>
</div>

<table id="statisticsTable">
    <caption><h3 class="title">More numbers</h3></caption>
    <tr>
        <th scope="row">Sum of the parking cars</th>
        <td id="sumCars">100</td>
    </tr>
    <tr>
        <th scope="row">Sum of parking trucks</th>
        <td id="sumTrucks">100</td>
    </tr>
    <tr>
        <th scope="row">Sum of parking Bikes</th>
        <td id="sumBikes">100</td>
    </tr>
    <tr>
        <th scope="row">Sum of leaving Vehicles</th>
        <td id="sumLeavingVehicles">100</td>
    </tr>
    <tr>
        <th scope="row">Collected money in general</th>
        <td id="sumCollected">100</td>
    </tr>
</table>


<footer>
    <h3>Authors (Team 37)</h3>
    <div>Minh Truong: <a href="mailto:minh.truong@smail.inf.h-brs.de">minh.truong@smail.inf.h-brs.de</a></div>
    <div>Ameur Khemissi: <a href="mailto:ameur.khemissi@smail.inf.h-brs.de">ameur.khemissi@smail.inf.h-brs.de</a></div>
</footer>



</body>
</html>
