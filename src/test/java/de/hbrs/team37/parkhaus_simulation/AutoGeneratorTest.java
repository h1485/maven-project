package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;

import java.security.SecureRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.jupiter.api.Test;

public class AutoGeneratorTest {
    String license;
    Vehicle car;
    String selected;

    @BeforeEach
    public void init(){
        license = AutoGenerator.generateLicense();
        String[] cartypes = MultitonVehicleTypes.getNames();
        selected = cartypes[new SecureRandom().nextInt(cartypes.length)];
        car = AutoGenerator.generateCar(selected);
    }

    @Test
    public void invalidCustomerType(){
        car = AutoGenerator.generateCar(MultitonVehicleTypes.getCarInstance().getName(), "sfdaskda");
        Assertions.assertNotNull(car);
    }

    @Test
    public void unknownCarType(){
        car = AutoGenerator.generateCar("SAFHISKAD");
        Assertions.assertTrue(car instanceof Car);
    }

    @RepeatedTest(30)
    public void correctInstances(){
        boolean check = false;
        String foundType = "";
        MultitonVehicleTypes vehicleType = MultitonVehicleTypes.getInstance(selected);
        if(vehicleType != null) foundType = vehicleType.getName();
        switch(foundType){
            case "Truck":
                check = car instanceof Truck;
                break;
            case "Bike":
                check = car instanceof Bike;
                break;
            default:
                check = car instanceof Car;
                break;
        }
        Assertions.assertTrue(check);
    }

    @RepeatedTest(30)
    public void licenseLengthBoundaries(){
        license = license.replace("-", "");
        Assertions.assertTrue(license.length()<= 8);
        Assertions.assertTrue(license.length() >= 3);
    }

    @RepeatedTest(30)
    public void validCharacters(){
        String rules = "([A-Z]{1,3})-" +
                "([A-Z]{1,2})-" +
                "(\\d{1,4})";

        Pattern pattern = Pattern.compile(rules);
        Matcher matcher = pattern.matcher(license);
        if(matcher.find())
            Assertions.assertEquals(license, matcher.group(0));
        else {
            Assertions.fail();
        }
    }
}
