package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class BikeTest {

    Bike bike = new Bike("a", "Standard");

    @Test
    void testToString() {
        Assertions.assertEquals("Bike", bike.toString().substring(0,4));
    }
}