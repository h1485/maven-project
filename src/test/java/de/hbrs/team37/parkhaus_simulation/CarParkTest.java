package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CarParkTest {
    Clock clock;
    CarPark carPark;
    Vehicle v;

    @BeforeEach
    public void setUp() {
        clock = new Clock("10:00", "22:00", "10:00", 0, 1);
        carPark = new CarPark(3, clock);
        v = AutoGenerator.generateCar(MultitonVehicleTypes.getCarInstance().getName());
    }

    @Test
    public void enter() {
        Assertions.assertTrue(carPark.enter(v,2)); // enter in 5
        Assertions.assertNotNull(carPark.getPos(2)); // check if index 2 is occupied
        Assertions.assertFalse(carPark.enter(v,2));  // enter in 5 again (cannot happen bcz 5 is already taken)
        Assertions.assertFalse(carPark.enter(v,5)); // enter in x > capacity
        Assertions.assertFalse(carPark.enter(v,-1)); // enter in x < 0
    }

    @Test
    public void randomEnter() {
        String matcher = "\\d+,(Standard|Business|Family)";
        Assertions.assertTrue(carPark.randomEnter(MultitonVehicleTypes.getCarInstance().getName()).matches(matcher)); // park in random index
        Assertions.assertTrue(carPark.randomEnter(MultitonVehicleTypes.getCarInstance().getName()).matches(matcher)); // park in random index
        Assertions.assertTrue(carPark.randomEnter(MultitonVehicleTypes.getCarInstance().getName()).matches(matcher)); // park in random index
        Assertions.assertEquals(carPark.randomEnter(MultitonVehicleTypes.getCarInstance().getName()), "-1"); // all indexes are occupied
    }

    @Test
    public void leave() {
        Assertions.assertNull(carPark.leave(2));  //carpark is empty
        Assertions.assertNull(carPark.leave(5)); //5 >= capacity
        Assertions.assertNull(carPark.leave(-1)); //-1 is not a valid index
        carPark.enter(v,2);
        Assertions.assertNull(carPark.leave(1)); //no vehicle in index 1
        Assertions.assertNotNull(carPark.leave(2)); // should return vehicle
        Assertions.assertNull(carPark.leave(2)); //vehicle left in last line
    }

    @Test
    public void randomLeave() {
        Assertions.assertEquals(carPark.randomLeave(), -1);
        carPark.enter(v,2);
        Assertions.assertEquals(carPark.randomLeave(), 2); // it only returns a random occupied position
    }

    @Test
    public void getCapacity() {
        Assertions.assertEquals(carPark.getCapacity(), 3);
    }

    @Test
    public void getTakenParkingSpaces() {
        Assertions.assertEquals(carPark.getTakenParkingSpaces(), 0);
        carPark.enter(v, 1);
        Assertions.assertEquals(carPark.getTakenParkingSpaces(), 1);
    }

    @Test
    public void getFreeParkingSpaces() {
        Assertions.assertEquals(carPark.getFreeParkingSpaces(), 3);
        carPark.enter(v, 1);
        Assertions.assertEquals(carPark.getFreeParkingSpaces(), 2);
    }

    @Test
    public void testToString() {
        Assertions.assertEquals(carPark.toString(), "");
        carPark.enter(v, 1);
        carPark.enter(v, 2);
        Assertions.assertEquals(carPark.toString(), "1," + v.toString() + "|" + "2," + v.toString());
    }

    @Test
    public void getVehicles() {
        Assertions.assertEquals(carPark.getCapacity(), carPark.getVehicles().length);
    }

    @Test
    public void reset() {
        carPark.enter(v, 1);
        carPark.reset();
        Assertions.assertEquals(carPark.getFreeParkingSpaces(), 3);
        Assertions.assertEquals(carPark.getTakenParkingSpaces(), 0);
    }

    @Test
    public void increaseCapacity() {
        Assertions.assertEquals(carPark.getCapacity(), 3);
        carPark.increaseCapacity();
        Assertions.assertEquals(carPark.getCapacity(), 4);
    }

    @Test
    public void decreaseCapacity() {
        Assertions.assertEquals(carPark.getCapacity(), 3);
        carPark.decreaseCapacity();
        Assertions.assertEquals(carPark.getCapacity(), 2);
    }

    @Test
    public void getPos() {
        Assertions.assertNull(carPark.getPos(0));
        carPark.enter(v, 0);
        Assertions.assertEquals(carPark.getPos(0),v);
    }

    @Test
    public void getClock() {
        Assertions.assertEquals(carPark.getClock(), clock);
    }

    @Test
    public void setCapacity() {
        carPark.setCapacity(5);
        Assertions.assertEquals(carPark.getCapacity(), 5);
    }
}