package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CarTest {
    Car car = new Car("a", "Family");

    @Test
    void testToString() {
        Assertions.assertEquals("Car", car.toString().substring(0,3));
    }
}