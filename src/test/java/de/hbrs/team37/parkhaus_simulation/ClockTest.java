package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.concurrent.TimeUnit;

public class ClockTest {
    private static final String TIME_1 = "18:00";
    private static final String TIME_2 = "18:30";
    private static final String TIME_3 = "15:00";
    Clock clock = new Clock(TIME_1, "20:00", "18:01");


    @Test
    public void letterAsInput() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Clock("32323", "32dasd", "dasad"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Clock("dd:22", "32dasd", "dasad"));
    }

    @Test
    public void undefinedDomain() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Clock("25:00", TIME_1, "5:00"));
    }

    @Test
    public void interpretTime() {
        Assertions.assertEquals(18 * 60, Clock.interpretTimeString(TIME_1));
        Assertions.assertEquals(18 * 60 + 30, Clock.interpretTimeString(TIME_2));
    }

    @Test
    public void correctOpen() {
        Assertions.assertTrue(clock.open());
        clock = new Clock("19:00", TIME_1, "17:00");
        Assertions.assertTrue(clock.open());
        clock = new Clock(TIME_1, "20:00", "21:00");
        Assertions.assertFalse(clock.open());
        clock = new Clock("19:00", TIME_1, TIME_2);
        Assertions.assertFalse(clock.open());
    }

    @Test
    public void testOpenTime() {
        String anfang = TIME_1;
        String end = "23:00";
        clock = new Clock(anfang, end, "8:00");
        Assertions.assertEquals(clock.getOpenHour(), anfang + "," + end);

        anfang = "18:50";
        end = "23:30";
        clock = new Clock(anfang, end, "8:00");
        Assertions.assertEquals(clock.getOpenHour(), anfang + "," + end);
    }

    @Test
    public void correctSimulationSpeed() {
        Assertions.assertEquals(1, Clock.getSimulationSpeed());
        clock = new Clock(TIME_1, TIME_3, "3:00", 1, 5);
        Assertions.assertEquals(5, Clock.getSimulationSpeed());
    }

    @Test
    public void clockBackgroundTask() throws InterruptedException {
        clock = new Clock(TIME_3, "15:10", TIME_3, 1, 1);
        TimeUnit.SECONDS.sleep(2);
        Assertions.assertTrue(clock.open());
        TimeUnit.SECONDS.sleep(9);
        Assertions.assertFalse(clock.open());
    }

    @Test
    public void checkSimulationSpeed() throws InterruptedException {
        clock = new Clock(TIME_1, TIME_2, "18:01", 1, 2);
        Assertions.assertTrue(clock.open());
        TimeUnit.SECONDS.sleep(16);
        Assertions.assertFalse(clock.open());
    }

    @Test
    public void checkDays() {
        String[] days = {"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su", "Mo"};
        for (int i = 0; i < 8; i++) {
            clock = new Clock("5:00", "5:00", "5:00", i);
            Assertions.assertEquals(days[i], clock.getDay());
        }
    }

    @Test
    public void checkDayOverflow() throws InterruptedException {
        clock = new Clock("23:59", "5:00", "23:59", 6);
        TimeUnit.SECONDS.sleep(2);
        Assertions.assertTrue(Clock.interpretTimeString(clock.getCurrentTime()) >= 0 &&
                                       Clock.interpretTimeString(clock.getCurrentTime()) < 5);
        TimeUnit.SECONDS.sleep(2);
        Assertions.assertEquals("Mo", clock.getDay());

    }

    @Test
    public void correctShutdown(){
        Assertions.assertTrue(Clock.getThreadState());
        Clock.stopThread();
        Assertions.assertFalse(Clock.getThreadState());
        Clock.initThread();
        Assertions.assertTrue(Clock.getThreadState());

    }
}
