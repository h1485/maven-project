package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.servlet.ServletException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DatamanagerTest {
    CarPark carPark;
    @BeforeEach
    public void init(){
        SingletonCarHistory.getInstance().reset();
        carPark = new CarPark(10, new Clock("10:00", "22:00", "12:00", 0, 1/5));
        for(int i = 0; i < 5; i++){
            carPark.randomEnter(MultitonVehicleTypes.getCarInstance().getName());
        }
    }

    @AfterAll
    public static void cleanUp() throws IOException {
        Files.delete(Paths.get(AbstractParkhausServlet.HISTORY_SAVE_LOCATION));
        Logger.getLogger("Datamanager History").log(Level.WARNING, "File deletion failed");
        for (MultitonVehicleTypes vehicleType : MultitonVehicleTypes.getAllInstances()) {
            try {
                Path path = Paths.get(vehicleType.getPath());
                Files.delete(path);
            } catch (NoSuchFileException e) {
                Logger.getLogger("DatamanagerTest").log(Level.INFO, "File not found");
            }
        }
    }

    @Test
    public void saveLoadCarParkTest() throws IOException {
        final String testPath = "Text.txt";
        Datamanager.saveCarPark(testPath, carPark);
        CarPark testPark = new CarPark(10, new Clock("10:00", "22:00", "15:00", 0, 1/5));
        Datamanager.loadCarPark(testPath, testPark);

        for(int i = 0; i < carPark.getCapacity(); i++){
            if(testPark.getPos(i) != null)
                Assertions.assertEquals(carPark.getPos(i).toString(), testPark.getPos(i).toString());
        }
        try {
            Files.delete(Paths.get(testPath));
        }catch(NoSuchFileException e){
        Logger.getLogger("DatamanagerTest").log(Level.INFO, "File not found");
    }
    }

    @Test
    public void saveLoadHistory() throws IOException{
        carPark.randomLeave();
        carPark.randomLeave();
        List<Vehicle> copylist = new ArrayList<>(SingletonCarHistory.getInstance().getCarHistory());
        Datamanager.saveHistory();
        SingletonCarHistory.getInstance().reset();
        Assertions.assertTrue(SingletonCarHistory.getInstance().getCarHistory().isEmpty());
        Datamanager.loadHistory();


        for(int i = 0; i<copylist.size(); i++){
            Assertions.assertEquals(copylist.get(i).toString(), SingletonCarHistory.getInstance().getCarHistory().get(i).toString());
        }
    }

    @Test
    public void parseDataTest(){
        Vehicle vehicle = new Car(AutoGenerator.generateLicense(), MultitonCustomerTypes.getStandardInstance().getName());
        Vehicle parsedVehicle = null;

        try{
            parsedVehicle = Datamanager.parseData(vehicle.storeFormat());
        }catch (Exception e){
            Assertions.fail();
        }
        Assertions.assertNotNull(parsedVehicle);
        if (parsedVehicle != null)
            Assertions.assertEquals(vehicle.toString(), parsedVehicle.toString());
    }

    @Test
    public void loadCarParkOnStartup() throws ServletException {

        for (int i = 0; i < MultitonVehicleTypes.getNames().length; i++) {
            AbstractParkhausServlet abstractParkhausServlet = new ServletCar();
            abstractParkhausServlet.init();
            Clock.setSimulationSpeed(1 / 5);
            Vehicle vehicle;
            Vehicle vehicle2;

            switch (i) {
                case 1:
                    vehicle = new Truck(AutoGenerator.generateLicense(), MultitonCustomerTypes.getStandardInstance().getName());
                    vehicle2 = new Truck(AutoGenerator.generateLicense(), MultitonCustomerTypes.getBusinessInstance().getName());
                    abstractParkhausServlet = new ServletTruck();
                    abstractParkhausServlet.init();
                    break;
                case 2:
                    vehicle = new Bike(AutoGenerator.generateLicense(), MultitonCustomerTypes.getStandardInstance().getName());
                    vehicle2 = new Bike(AutoGenerator.generateLicense(), MultitonCustomerTypes.getBusinessInstance().getName());
                    abstractParkhausServlet = new ServletBike();
                    abstractParkhausServlet.init();
                    break;
                default:
                    vehicle = new Car(AutoGenerator.generateLicense(), MultitonCustomerTypes.getStandardInstance().getName());
                    vehicle2 = new Car(AutoGenerator.generateLicense(), MultitonCustomerTypes.getFamilyInstance().getName());
            }
            abstractParkhausServlet.getCarPark().reset();
            abstractParkhausServlet.getCarPark().enter(vehicle, 0);
            abstractParkhausServlet.getCarPark().enter(vehicle2, 1);
            abstractParkhausServlet.destroy();
            abstractParkhausServlet.init();

            Assertions.assertEquals(vehicle.toString(), abstractParkhausServlet.getCarPark().getPos(0).toString());
            Assertions.assertEquals(vehicle2.toString(), abstractParkhausServlet.getCarPark().getPos(1).toString());
        }
    }

}
