package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HelperClassTest {
    String rgbaMatcher = "rgba[(]\\d{1,3},\\d{1,3},\\d{1,3},\\d[.]\\d[)]";
    String hslaMatcher = "hsla[(]\\d{1,3},\\d{1,3}%,\\d{1,3}%,\\d[.]\\d[)]";
    String validJsonString = "{ \"developers\": [{ \"firstName\":\"Linus\" , \"lastName\":\"Torvalds\" }, " +
            "{ \"firstName\":\"John\" , \"lastName\":\"von Neumann\" } ]}";
    String invalidJsonString = "\"developers\": [ \"firstName\":\"Linus\" , \"lastName\":\"Torvalds\" }, " +
            "{ \"firstName\":\"John\" , \"lastName\":\"von Neumann\" } ]}";

    @Test
    void toHex() {
        assertEquals("F458BC", HelperClass.toHex(new byte[]{(byte)500,(byte)600,(byte)700}));
    }

    @Test
    void round() {
        assertEquals(5.12346, HelperClass.round( 5.123456789, 5));
        assertEquals(5.1235, HelperClass.round( 5.123456789, 4));
        assertEquals(5.123, HelperClass.round( 5.123456789, 3));
    }

    @Test
    void generateColorRGB() {
        for (String color : HelperClass.generateColorRGB(100,1)) {
            assertTrue(color.matches(rgbaMatcher));
        }
    }

    @Test
    void generateColorHSLA() {
        for (String color : HelperClass.generateColorHSLA(100,1.0, true)){
            assertTrue(color.matches(hslaMatcher));
        }
    }

    @Test
    void generateShadesRGB() {
        for (String color: HelperClass.generateShadesRGB("rgba(200,150,200,1.0)", 100, false)){
            assertTrue(color.matches(rgbaMatcher));
        }
    }

    @Test
    void generateShadesRGBTint() {
        for (String color: HelperClass.generateShadesRGB("rgba(200,150,200,1.0)", 100, true)){
            assertTrue(color.matches(rgbaMatcher));
        }
    }

    @Test
    void changeAlpha() {
        String[] colors = HelperClass.generateColorHSLA(10,0.5,true);
        for (String color: HelperClass.changeAlpha(colors, 0.7)){
           assertTrue(color.matches(hslaMatcher));
        }
    }

    @Test
    void normalizeDuration() {
        assertEquals("1d 1h 5min", HelperClass.normalizeDuration(1505));
        assertEquals("1h 1min", HelperClass.normalizeDuration(61));
        assertEquals("1min", HelperClass.normalizeDuration(1));
    }

    @Test
    void isJSONValid(){
        assertTrue(HelperClass.isJSONValid(validJsonString));
        assertFalse(HelperClass.isJSONValid(invalidJsonString));
    }
}