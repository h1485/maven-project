package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.*;

class JsonChartGeneratorTest {
    JsonChartGenerator jsonGen;
    JsonChartGenerator.DataSet data;
    JsonChartGenerator.DataSet data2;
    JsonChartGenerator.DataSet data3;
    String[] supportedTypes = {"line", "bar", "radar", "doughnut",
            "pie", "polarArea", "bubble", "scatter"};

    @BeforeEach
    void setUp() {
        jsonGen = new JsonChartGenerator();
        data = jsonGen.createDataSet(new double[]{1.0, 2.0, 3.0});
        data2 = jsonGen.createDataSet(new double[]{1.0, 2.0, 3.0},
                new double[]{1.0, 2.0, 3.0});
        data3 = jsonGen.createDataSet(new double[]{1.0, 2.0, 3.0},
                new double[]{1.0, 2.0, 3.0},
                new double[]{1.0, 2.0, 3.0});
    }


    @Test
    void addDataSet() {
        jsonGen.addDataSet(data);
        assertEquals(data, jsonGen.getDataSets().get(0));
    }

    @Test
    void createDataSet() {
        assertNotNull(data);
        assertNotNull(data2);
        assertNotNull(data3);

    }

    @Test
    void getDataSets() {
        jsonGen.addDataSet(data);
        jsonGen.addDataSet(data2);
        jsonGen.addDataSet(data3);
        assertEquals(3, jsonGen.getDataSets().size());
        assertEquals(data, jsonGen.getDataSets().get(0));
        assertEquals(data2, jsonGen.getDataSets().get(1));
        assertEquals(data3, jsonGen.getDataSets().get(2));

    }

    @Test
    void getData() {
        jsonGen.addDataSet(data);
        assertEquals(data, jsonGen.getData(0));
    }

    @Test
    void setType(){
        for(String type : supportedTypes) {
            assertTrue(jsonGen.setType(type));
        }
        assertFalse(jsonGen.setType("xx"));
    }

    @Test
    void generateChart() throws IOException {
        jsonGen.addDataSet(data);
        String chart = jsonGen.generateChart();
        String type = chart.split("\\|")[0];
        String jsonString = chart.split("\\|")[1];
        assertTrue(Arrays.asList(supportedTypes).contains(type));
        assertTrue(HelperClass.isJSONValid(jsonString));
    }

    @Test
    void sizeTest(){
        int[] size = data.size();
        Assertions.assertEquals(3, size[0]);
        Assertions.assertEquals(1, size[1]);
        Assertions.assertEquals(1, size[2]);

        size = data3.size();
        Assertions.assertEquals(3, size[0]);
        Assertions.assertEquals(3, size[1]);
        Assertions.assertEquals(3, size[2]);
    }

    @Test
    void testScatter() throws IOException {
        jsonGen.addDataSet(data2);
        data3.autoParameter();
        jsonGen.setType("scatter");
        String json = jsonGen.generateChart();
        String type = json.split("\\|")[0];
        json = json.split("\\|")[1];
        assertTrue(Arrays.asList(supportedTypes).contains(type));
        Assertions.assertTrue(HelperClass.isJSONValid(json));
    }

    @Test
    void testBubble() throws IOException{
        jsonGen.addDataSet(data3);
        data3.autoParameter();
        jsonGen.setType("bubble");
        String json = jsonGen.generateChart();
        String type = json.split("\\|")[0];
        json = json.split("\\|")[1];
        assertTrue(Arrays.asList(supportedTypes).contains(type));
        Assertions.assertTrue(HelperClass.isJSONValid(json));
    }
}