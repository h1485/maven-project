package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultitonCustomerTypesTest  {
    static final String STANDARD = "Standard";
    static final String FAMILY = "Family";
    static final String BUSINESS = "Business";

    @BeforeEach
    void setUp() {
        MultitonCustomerTypes.reset();
    }

    @Test
    void getName() {
       assertEquals(STANDARD, MultitonCustomerTypes.getStandardInstance().getName());
       assertEquals(FAMILY, MultitonCustomerTypes.getFamilyInstance().getName());
       assertEquals(BUSINESS, MultitonCustomerTypes.getBusinessInstance().getName());

    }

    @Test
    void getDiscount() {
        assertEquals(1.0, MultitonCustomerTypes.getStandardInstance().getDiscount());
        assertEquals(0.7, MultitonCustomerTypes.getFamilyInstance().getDiscount());
        assertEquals(0.3, MultitonCustomerTypes.getBusinessInstance().getDiscount());
    }

    @Test
    void setDiscount() {
        MultitonCustomerTypes.getStandardInstance().setDiscount(5.0);
        assertEquals(5.0, MultitonCustomerTypes.getStandardInstance().getDiscount());
    }


    @Test
    void getStandardInstance() {
        assertEquals(STANDARD, MultitonCustomerTypes.getStandardInstance().getName());
        assertEquals(1.0, MultitonCustomerTypes.getStandardInstance().getDiscount());
    }

    @Test
    void getFamilyInstance() {
        assertEquals(FAMILY, MultitonCustomerTypes.getFamilyInstance().getName());
        assertEquals(0.7, MultitonCustomerTypes.getFamilyInstance().getDiscount());
    }

    @Test
    void getBusinessInstance() {
        assertEquals(BUSINESS, MultitonCustomerTypes.getBusinessInstance().getName());
        assertEquals(0.3, MultitonCustomerTypes.getBusinessInstance().getDiscount());
    }

    @Test
    void getAllDiscounts() {
        assertEquals("1.0|0.7|0.3", MultitonCustomerTypes.getAllDiscounts());
    }

    @Test
    void reset() {
        MultitonCustomerTypes.getStandardInstance().setDiscount(5.0);
        MultitonCustomerTypes.getFamilyInstance().setDiscount(5.0);
        MultitonCustomerTypes.getBusinessInstance().setDiscount(5.0);
        MultitonCustomerTypes.reset();
        assertEquals(1.0, MultitonCustomerTypes.getStandardInstance().getDiscount());
        assertEquals(0.7, MultitonCustomerTypes.getFamilyInstance().getDiscount());
        assertEquals(0.3, MultitonCustomerTypes.getBusinessInstance().getDiscount());

    }
}