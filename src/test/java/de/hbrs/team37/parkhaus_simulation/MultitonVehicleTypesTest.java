package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class MultitonVehicleTypesTest {
    static final String CAR = "Car";
    static final String BIKE = "Bike";
    static final String TRUCK = "Truck";

    CarPark carPark = new CarPark(2, new Clock("10:00", "22:00", 1));
    Vehicle vehicle = new Car("123", "Standard");
    Vehicle vehicle2;

    @BeforeEach


    @Test
    void getName() {
        assertEquals(CAR, MultitonVehicleTypes.getCarInstance().getName());
        assertEquals(BIKE, MultitonVehicleTypes.getBikeInstance().getName());
        assertEquals(TRUCK, MultitonVehicleTypes.getTruckInstance().getName());
    }

    @Test
    void getPath() {
        assertEquals("./Car.txt", MultitonVehicleTypes.getCarInstance().getPath());
        assertEquals("./Bike.txt", MultitonVehicleTypes.getBikeInstance().getPath());
        assertEquals("./Truck.txt", MultitonVehicleTypes.getTruckInstance().getPath());
    }

    @Test
    void getInitialCapacity() {
        assertEquals(20,MultitonVehicleTypes.getCarInstance().getInitialCapacity());
        assertEquals(15,MultitonVehicleTypes.getTruckInstance().getInitialCapacity());
        assertEquals(10,MultitonVehicleTypes.getBikeInstance().getInitialCapacity());
    }

    @Test
    void getBasePrice() {
        assertEquals(2.0, MultitonVehicleTypes.getCarInstance().getBasePrice());
        assertEquals(15.0,MultitonVehicleTypes.getTruckInstance().getBasePrice());
        assertEquals(1.0,MultitonVehicleTypes.getBikeInstance().getBasePrice());
    }

    @Test
    void setBasePrice() throws InterruptedException {
        carPark.enter(vehicle,0);
        MultitonVehicleTypes.getCarInstance().setBasePrice(5.0);
        vehicle2 = new Car("1234","Standard");
        carPark.enter(vehicle2,1);
        TimeUnit.SECONDS.sleep(2);
        carPark.leave(0);
        carPark.leave(1);
        assertTrue(vehicle2.getPrice() > 2 * vehicle.getPrice());
    }


    @Test
    void getNames() {
        assertEquals(3, MultitonVehicleTypes.getNames().length);
        assertEquals(CAR, MultitonVehicleTypes.getNames()[0]);
        assertEquals(TRUCK, MultitonVehicleTypes.getNames()[1]);
        assertEquals(BIKE, MultitonVehicleTypes.getNames()[2]);
    }

    @Test
    void getVehicleTypesRegex() {
        assertEquals("(Car|Truck|Bike|\b)", MultitonVehicleTypes.getVehicleTypesRegex());
    }

    @Test
    void testToString() {
        assertEquals(CAR, MultitonVehicleTypes.getCarInstance().toString());
        assertEquals(BIKE, MultitonVehicleTypes.getBikeInstance().toString());
        assertEquals(TRUCK, MultitonVehicleTypes.getTruckInstance().toString());
    }

    @Test
    void getCarInstance() {
        assertEquals(CAR,MultitonVehicleTypes.getCarInstance().getName());
        assertEquals("./Car.txt",MultitonVehicleTypes.getCarInstance().getPath());
        assertEquals(20,MultitonVehicleTypes.getCarInstance().getInitialCapacity());
    }

    @Test
    void getTruckInstance() {
        assertEquals(TRUCK,MultitonVehicleTypes.getTruckInstance().getName());
        assertEquals("./Truck.txt",MultitonVehicleTypes.getTruckInstance().getPath());
        assertEquals(15,MultitonVehicleTypes.getTruckInstance().getInitialCapacity());
    }

    @Test
    void getBikeInstance() {
        assertEquals(BIKE,MultitonVehicleTypes.getBikeInstance().getName());
        assertEquals("./Bike.txt",MultitonVehicleTypes.getBikeInstance().getPath());
        assertEquals(10,MultitonVehicleTypes.getBikeInstance().getInitialCapacity());
    }

    @Test
    void getAllInstances() {
        assertEquals(3, MultitonVehicleTypes.getAllInstances().length);
        assertEquals(MultitonVehicleTypes.getCarInstance(), MultitonVehicleTypes.getAllInstances()[0]);
        assertEquals(MultitonVehicleTypes.getTruckInstance(), MultitonVehicleTypes.getAllInstances()[1]);
        assertEquals(MultitonVehicleTypes.getBikeInstance(), MultitonVehicleTypes.getAllInstances()[2]);

    }

    @Test
    void getInstance() {
        assertEquals(MultitonVehicleTypes.getCarInstance(), MultitonVehicleTypes.getInstance(CAR));
        assertEquals(MultitonVehicleTypes.getBikeInstance(), MultitonVehicleTypes.getInstance(BIKE));
        assertEquals(MultitonVehicleTypes.getTruckInstance(), MultitonVehicleTypes.getInstance(TRUCK));
    }

    @Test
    void reset() {
        MultitonVehicleTypes.getCarInstance().setBasePrice(1.0);
        MultitonVehicleTypes.getCarInstance().setBasePrice(1.0);
        MultitonVehicleTypes.getCarInstance().setBasePrice(1.0);
        MultitonVehicleTypes.reset();
        assertEquals(2.0, MultitonVehicleTypes.getCarInstance().getBasePrice());
        assertEquals(15.0,MultitonVehicleTypes.getTruckInstance().getBasePrice());
        assertEquals(1.0,MultitonVehicleTypes.getBikeInstance().getBasePrice());
    }

}