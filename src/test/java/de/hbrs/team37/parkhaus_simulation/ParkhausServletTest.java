package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.json.*;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.mockito.Mockito;

public class ParkhausServletTest extends Mockito{
    private AbstractParkhausServlet parkhausServlet;
    private HttpServletResponse response;
    private HttpServletRequest request;
    private final StringWriter stringWriter = new StringWriter();
    private final PrintWriter printWriter = new PrintWriter(stringWriter);
    private static final Logger logger = Logger.getLogger("ServletTest");

    private static class ServletInputStreamInstance extends ServletInputStream{
        private final byte[] byteArray;
        private ReadListener listener = null;
        private int lastIndex = -1;


        public ServletInputStreamInstance(byte[] byteArray){
            this.byteArray = byteArray;
        }

        @Override
        public boolean isFinished() {
            return lastIndex == byteArray.length-1;
        }

        @Override
        public boolean isReady() {
            return isFinished();
        }

        @Override
        public void setReadListener(ReadListener readListener) {
            this.listener = readListener;
            if(!isFinished()) {
                try {
                    readListener.onDataAvailable();
                } catch (IOException e) {
                    readListener.onError(e);
                }
            }else {
                try {
                    readListener.onAllDataRead();
                } catch (IOException e) {
                    readListener.onError(e);
                }
            }
        }

        @Override
        public int read(){
            int i;
            if (!isFinished()) {
                i = byteArray[lastIndex+1];
                lastIndex++;
                if (isFinished() && (listener != null)) {
                    try {
                        listener.onAllDataRead();
                    } catch (IOException e) {
                        listener.onError(e);
                        try {
                            throw e;
                        } catch (IOException ioException) {
                                    logger.log(Level.WARNING, "Could not read ServletOutputStream");
                        }
                    }
                }
                return i;
            } else {
                return -1;
            }
        }
    }

    @AfterAll
    static void cleanUp() throws IOException {
        SingletonCarHistory.getInstance().reset();
        try {
            for (MultitonVehicleTypes vehicleType : MultitonVehicleTypes.getAllInstances()) {
                Path path = Paths.get(vehicleType.getPath());
                Files.delete(path);
            }
        }catch(NoSuchFileException e){
            logger.log(Level.INFO, "File not found in cleanUp");
        }

    }

    @BeforeEach
    public void init() throws ServletException, IOException {
        stringWriter.getBuffer().setLength(0);
        SingletonCarHistory.getInstance().reset();
        Datamanager.saveHistory();
        parkhausServlet = new ServletCar();
        parkhausServlet.init();
        parkhausServlet.getCarPark().reset();
        parkhausServlet.destroy();
        parkhausServlet.init();
        response = mock(HttpServletResponse.class);
        request = mock(HttpServletRequest.class);
        when(response.getWriter()).thenReturn(printWriter);
        Clock.setSimulationSpeed(1.0/5);

    }

    private void inputCarSimulation(int n) throws IOException{
        if(n > parkhausServlet.getCarPark().getCapacity())
            n = parkhausServlet.getCarPark().getCapacity();

        byte[] byteString = "randomEnter".getBytes(StandardCharsets.UTF_8);
        for(int i = 0; i<n; i++) {
            ServletInputStream is = new ServletInputStreamInstance(byteString);
            when(request.getInputStream()).thenReturn(is);
            parkhausServlet.doPost(request, response);
        }
    }

    private void leaveCarSimulation(int n) throws IOException{
        if(n > parkhausServlet.getCarPark().getCapacity())
            n = parkhausServlet.getCarPark().getCapacity();

        byte[] byteString = "randomLeave".getBytes(StandardCharsets.UTF_8);
        for(int i = 0; i<n; i++) {
            ServletInputStream is = new ServletInputStreamInstance(byteString);
            when(request.getInputStream()).thenReturn(is);
            parkhausServlet.doPost(request, response);
        }
    }


    //Testing of doGet-Method

    @Test
    public void checkInitSize(){
        Assertions.assertEquals(20, parkhausServlet.max());
    }

    @Test
    public void correctConfig(){
        stringWriter.getBuffer().setLength(0);
        when(request.getParameter("cmd")).thenReturn("config");
        parkhausServlet.doGet(request, response);
        Assertions.assertEquals(parkhausServlet.config(), stringWriter.toString());
    }


    @Test
    public void checkSumFunctions() throws IOException, InterruptedException{
        List<Vehicle> vehicleHistory = parkhausServlet.getCarHistory();
        CarPark carPark = parkhausServlet.getCarPark();
        StatisticCollector statistics = new StatisticCollector(vehicleHistory);

        double sumCarPark = 0;
        long storedTime = 0;

        inputCarSimulation(10);
        TimeUnit.SECONDS.sleep(2);
        leaveCarSimulation(2);


        for(Vehicle v : carPark) {
            sumCarPark += v.getPrice();
        }

        for(Vehicle v : vehicleHistory) {
            storedTime += v.getTicket().getTime();
        }
        sumCarPark = HelperClass.round(sumCarPark,2);

        stringWriter.getBuffer().setLength(0);
        Assertions.assertEquals(sumCarPark, statistics.getCurrentSum(carPark));
        when(request.getParameter("cmd")).thenReturn("getCurrentSum");
        parkhausServlet.doGet(request,response);
        Assertions.assertEquals(sumCarPark+"", stringWriter.toString());

        Assertions.assertEquals(storedTime, statistics.getStoredTime());

        stringWriter.getBuffer().setLength(0);
        when(request.getParameter("cmd")).thenReturn("getCollectedSum");
        parkhausServlet.doGet(request,response);

        stringWriter.getBuffer().setLength(0);
        when(request.getParameter("cmd")).thenReturn("getAllSum");
        parkhausServlet.doGet(request,response);

        stringWriter.getBuffer().setLength(0);
        when(request.getParameter("cmd")).thenReturn("getTimeSum");
        parkhausServlet.doGet(request,response);
        Assertions.assertEquals((storedTime)+"m", stringWriter.toString());

    }

    @Test
    public void averageCMD() throws IOException, InterruptedException{


        //Check for length 0 average call in doGet
        stringWriter.getBuffer().setLength(0);
        when(request.getParameter("cmd")).thenReturn("average");
        parkhausServlet.doGet(request, response);
        Assertions.assertEquals("0,0", stringWriter.toString());

        inputCarSimulation(10);
        TimeUnit.SECONDS.sleep(5);
        leaveCarSimulation(5);

        List<Vehicle> vehicleList = parkhausServlet.getCarHistory();
        StatisticCollector statistics = new StatisticCollector(vehicleList);

        String assertionString =   HelperClass.round(statistics.getStoredSum()/vehicleList.size(),2)+ ","
                                + HelperClass.round(statistics.getStoredTime()/(double) vehicleList.size(),2);

        stringWriter.getBuffer().setLength(0);
        when(request.getParameter("cmd")).thenReturn("average");
        parkhausServlet.doGet(request, response);

        Assertions.assertEquals(assertionString, stringWriter.toString());
    }

    @Test
    public void correctCarString() throws IOException{
        inputCarSimulation(5);
        stringWriter.getBuffer().setLength(0);
        when(request.getParameter("cmd")).thenReturn("cars");
        parkhausServlet.doGet(request,response);

        String regexRules = new StringBuilder("(\\d+),")
                .append(MultitonVehicleTypes.getVehicleTypesRegex()).append(",")
                .append("(\\w{1,3}\\-\\w{1,2}\\-\\d{1,4}),")
                .append(MultitonCustomerTypes.getCustomerRegex()).append(",")
                .append("(.{10}),")
                .append("(\\d+\\.\\d+),")
                .append("(\\d+min),")
                .append("(\\w{2} \\d{1,2}\\:\\d{2}),")
                .append("(Parking)").toString();

        String[] cars = stringWriter.toString().split("\\|");

        for(String car: cars) {
            Assertions.assertTrue(car.matches(regexRules));
        }
    }

    @Test
    public void getCurrentTime() {
        when(request.getParameter("cmd")).thenReturn("currentTime");
        parkhausServlet.doGet(request,response);
        Assertions.assertEquals(parkhausServlet.getCarPark().getClock().getDate()+"",stringWriter.toString());
    }

    @Test
    public void checkOpen(){
        when(request.getParameter("cmd")).thenReturn("open");
        parkhausServlet.doGet(request,response);
        Assertions.assertEquals(parkhausServlet.getCarPark().getClock().open()+"",stringWriter.toString());
    }

    @Test
    public void checkLeavingCars() throws IOException{
        inputCarSimulation(10);
        leaveCarSimulation(5);

        List<Vehicle> vehicleArrayList = parkhausServlet.getCarHistory();
        Vehicle[] filteredLeft = vehicleArrayList.stream().filter(vehicle ->
                vehicle.getTicket().timeLeft.equals("Parking")).toArray(Vehicle[]::new);

        Assertions.assertEquals(10, vehicleArrayList.size());
        Assertions.assertEquals(5, filteredLeft.length);


        String regexRules = new StringBuilder(MultitonVehicleTypes.getVehicleTypesRegex()).append(",")
                .append("(\\w{1,3}\\-\\w{1,2}\\-\\d{1,4}),")
                .append(MultitonCustomerTypes.getCustomerRegex()).append(",")
                .append("(.{10}),")
                .append("(\\d+\\.\\d+),")
                .append("(\\d+min),")
                .append("(\\w{2} \\d{1,2}\\:\\d{2}),")
                .append("(\\w{2} \\d{1,2}\\:\\d{2})").toString();

        when(request.getParameter("cmd")).thenReturn("leavingCars");
        stringWriter.getBuffer().setLength(0);
        parkhausServlet.doGet(request,response);

        String[] cars = stringWriter.toString().split("\\|");

        for(String car: cars) {
            Assertions.assertTrue(car.matches(regexRules));
        }
    }

    @Test
    public void checkValidJsonChart() throws IOException{
        inputCarSimulation(10);
        leaveCarSimulation(5);

        stringWriter.getBuffer().setLength(0);
        when(request.getParameter("cmd")).thenReturn("chartEarnings");
        parkhausServlet.doGet(request,response);
        String jsonString = stringWriter.toString().replaceAll("(\\|?\\w+\\|)", "");
        String[] jsonStringArray = jsonString.split("\\|");

        stringWriter.getBuffer().setLength(0);
        when(request.getParameter("cmd")).thenReturn("chartNumbers");
        parkhausServlet.doGet(request,response);
        String jsonChartTwo = stringWriter.toString().replaceAll("(\\|?\\w+\\|)", "");
        String[] jsonStringChartTwo = jsonChartTwo.split("\\|");
        try{
            for(String json : jsonStringArray) {
                JsonReader jsonReader = Json.createReader(new StringReader(json));
                jsonReader.readValue();
            }

            for(String json: jsonStringChartTwo){
                JsonReader jsonReader = Json.createReader(new StringReader(json));
                jsonReader.readValue();
            }

        }catch (Exception e) {
            Assertions.fail();
        }
    }


    @Test
    public void reset() throws IOException{
        byte[] command = "reset".getBytes(StandardCharsets.UTF_8);
        ServletInputStream is = new ServletInputStreamInstance(command);
        when(request.getInputStream()).thenReturn(is);
        parkhausServlet.doPost(request,response);

        Assertions.assertEquals(0,parkhausServlet.getCarPark().getTakenParkingSpaces());
    }

    @Test
    public void resetAll() throws IOException{
        byte[] command = "resetAll".getBytes(StandardCharsets.UTF_8);
        ServletInputStream is = new ServletInputStreamInstance(command);
        when(request.getInputStream()).thenReturn(is);
        parkhausServlet.doPost(request,response);
        Assertions.assertEquals(0,parkhausServlet.getCarPark().getTakenParkingSpaces());
    }

    @Test
    public void unknownCommand(){
        when(request.getParameter("cmd")).thenReturn("sasdsadsa");
        parkhausServlet.doGet(request,response);
        Assertions.assertEquals("", stringWriter.toString());
    }

    //Testing doPost-Methods

    @Test
    public void enterTest() throws IOException{
        byte[] command = "enter,1,Business".getBytes(StandardCharsets.UTF_8);
        ServletInputStream is = new ServletInputStreamInstance(command);
        when(request.getInputStream()).thenReturn(is);
        parkhausServlet.doPost(request,response);
        CarPark carPark = parkhausServlet.getCarPark();

        Assertions.assertNotNull(carPark.getPos(1));
        Vehicle vehicle = carPark.getPos(1);
        Assertions.assertEquals(1, carPark.getTakenParkingSpaces());
        Assertions.assertEquals("Business", vehicle.getCustomerType());

        while(carPark.getTakenParkingSpaces() < carPark.getCapacity())
            inputCarSimulation(1);

        stringWriter.getBuffer().setLength(0);
        command = "enter,3,Family".getBytes(StandardCharsets.UTF_8);
        is = new ServletInputStreamInstance(command);
        when(request.getInputStream()).thenReturn(is);
        parkhausServlet.doPost(request,response);
        Assertions.assertEquals("No parking space left", stringWriter.toString());
    }

    @Test
    public void randomEnter() throws IOException{
        CarPark carPark = parkhausServlet.getCarPark();
        while(carPark.getTakenParkingSpaces() < carPark.getCapacity())
            inputCarSimulation(1);

        stringWriter.getBuffer().setLength(0);
        inputCarSimulation(1);
        Assertions.assertEquals("-1", stringWriter.toString());
    }

    @Test
    public void payTicket() throws IOException{
        CarPark carPark = parkhausServlet.getCarPark();
        int pos = Integer.parseInt(carPark.randomEnter(parkhausServlet.carType).split(",")[0]);
        Vehicle vehicle = carPark.getPos(pos);
        carPark.leave(pos);
        String command = "payTicket," + vehicle.getTicket().getHashcode().substring(10,20).toUpperCase();
        byte[] byteCommand = command.getBytes(StandardCharsets.UTF_8);
        ServletInputStream servletInputStream = new ServletInputStreamInstance(byteCommand);
        when(request.getInputStream()).thenReturn(servletInputStream);
        parkhausServlet.doPost(request, response);
        Assertions.assertEquals("paid", stringWriter.toString());


        stringWriter.getBuffer().setLength(0);
        byteCommand = "payTicket,AAAAAAA".getBytes(StandardCharsets.UTF_8);
        servletInputStream = new ServletInputStreamInstance(byteCommand);
        when(request.getInputStream()).thenReturn(servletInputStream);
        parkhausServlet.doPost(request,response);
        Assertions.assertEquals("no such ticket", stringWriter.toString());
    }

    @Test
    public void changeCapacity() throws IOException{
        byte[] byteCommand = "changeCapacity,35".getBytes(StandardCharsets.UTF_8);
        ServletInputStream servletInputStream = new ServletInputStreamInstance(byteCommand);
        when(request.getInputStream()).thenReturn(servletInputStream);
        parkhausServlet.doPost(request,response);
        Assertions.assertEquals(35, parkhausServlet.getCarPark().getCapacity());
    }

    @Test
    public void decreaseIncreaseCap() throws IOException{
        int initialCapacity = parkhausServlet.getCarPark().getCapacity();
        byte[] byteCommand = "decrease".getBytes(StandardCharsets.UTF_8);
        ServletInputStream servletInputStream = new ServletInputStreamInstance(byteCommand);
        when(request.getInputStream()).thenReturn(servletInputStream);
        parkhausServlet.doPost(request,response);
        Assertions.assertEquals(initialCapacity-1, parkhausServlet.getCarPark().getCapacity());

        stringWriter.getBuffer().setLength(0);
        byteCommand = "increase".getBytes(StandardCharsets.UTF_8);
        servletInputStream = new ServletInputStreamInstance(byteCommand);
        when(request.getInputStream()).thenReturn(servletInputStream);
        parkhausServlet.doPost(request, response);
        Assertions.assertEquals(initialCapacity, parkhausServlet.getCarPark().getCapacity());
    }

    @Test
    public void enableDisableCarPark() throws IOException{
        byte[] byteCommand = "disable".getBytes(StandardCharsets.UTF_8);
        ServletInputStream servletInputStream = new ServletInputStreamInstance(byteCommand);
        when(request.getInputStream()).thenReturn(servletInputStream);
        parkhausServlet.doPost(request,response);

        Assertions.assertFalse( parkhausServlet.getCarPark().isAvailable());

        byteCommand = "randomEnter".getBytes(StandardCharsets.UTF_8);
        servletInputStream = new ServletInputStreamInstance(byteCommand);
        when(request.getInputStream()).thenReturn(servletInputStream);
        stringWriter.getBuffer().setLength(0);
        parkhausServlet.doPost(request,response);
        Assertions.assertEquals("notAvailable", stringWriter.toString());

        byteCommand = "enable".getBytes(StandardCharsets.UTF_8);
        servletInputStream = new ServletInputStreamInstance(byteCommand);
        when(request.getInputStream()).thenReturn(servletInputStream);
        parkhausServlet.doPost(request,response);
        Assertions.assertTrue(parkhausServlet.getCarPark().isAvailable());
    }

    @Test
    public void simulationSpeedCheck() throws IOException{
        byte[] command = "simulationSpeed,1/60.0".getBytes(StandardCharsets.UTF_8);
        ServletInputStream servletInputStream = new ServletInputStreamInstance(command);
        when(request.getInputStream()).thenReturn(servletInputStream);
        parkhausServlet.doPost(request,response);
        Assertions.assertEquals(1.0/60, Clock.getSimulationSpeed());

        stringWriter.getBuffer().setLength(0);

        command = "simulationSpeed,15".getBytes(StandardCharsets.UTF_8);
        servletInputStream = new ServletInputStreamInstance(command);
        when(request.getInputStream()).thenReturn(servletInputStream);
        parkhausServlet.doPost(request,response);

        Assertions.assertEquals(15, Clock.getSimulationSpeed());
    }

    @Test
    public void checkDay() throws IOException{
        byte[] command = "clockDay,6".getBytes(StandardCharsets.UTF_8);
        ServletInputStream servletInputStream = new ServletInputStreamInstance(command);
        when(request.getInputStream()).thenReturn(servletInputStream);
        parkhausServlet.doPost(request,response);
        Assertions.assertEquals("Su", parkhausServlet.getCarPark().getClock().getDay());
    }

    @Test
    public void tomcatOutput() throws IOException{
        byte[] command = "tomcat".getBytes(StandardCharsets.UTF_8);
        ServletInputStream servletInputStream = new ServletInputStreamInstance(command);
        when(request.getInputStream()).thenReturn(servletInputStream);
        parkhausServlet.doPost(request,response);
        String rules = "Apache Tomcat/\\d+\\.\\d+\\.\\d+|Servlet is offline";
        Assertions.assertTrue(stringWriter.toString().matches(rules));
    }

    @Test
    public void unknownRequest() throws IOException{
        byte[] command = "asdfass".getBytes(StandardCharsets.UTF_8);
        ServletInputStream servletInputStream = new ServletInputStreamInstance(command);
        when(request.getInputStream()).thenReturn(servletInputStream);
        parkhausServlet.doPost(request,response);
        Assertions.assertEquals("", stringWriter.toString());
    }

    //Test class methods
    @Test
    public void carLeaveTest(){
        Vehicle vehicle = AutoGenerator.generateCar(MultitonVehicleTypes.getCarInstance().getName());
        parkhausServlet.getCarPark().enter(vehicle, 0);
        String[] params = new String[]{"leave","0"};
        parkhausServlet.carLeave(printWriter, params);
        Assertions.assertEquals("0," + vehicle.toString(), stringWriter.toString());
        stringWriter.getBuffer().setLength(0);

        parkhausServlet.carLeave(printWriter, params);
        Assertions.assertEquals("-1", stringWriter.toString());
    }

    @Test
    public void notAvailableEnter(){
        parkhausServlet.getCarPark().setAvailable(false);
        parkhausServlet.carEnter(printWriter, new String[]{""});
        Assertions.assertEquals("notAvailable", stringWriter.toString());
    }

    @Test
    public void onDestory(){
        Assertions.assertDoesNotThrow(parkhausServlet::destroy);
    }


}
