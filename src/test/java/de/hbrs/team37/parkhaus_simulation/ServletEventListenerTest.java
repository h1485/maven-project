package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.mockito.Mockito.*;

public class ServletEventListenerTest {
    //Object mocking thanks to https://stackoverflow.com/questions/44676287/how-to-mock-object-which-create-within-the-test-method
    ServletContextEvent event = Mockito.mock(ServletContextEvent.class);
    ServletContext context = Mockito.mock(ServletContext.class);
    ServletEventListener servletEventListener = new ServletEventListener();

    @BeforeEach
    public void setup(){
        when(event.getServletContext()).thenReturn(context);
        SingletonCarHistory.getInstance().reset();
    }

    @AfterAll
    static void cleanUp() throws IOException {
        SingletonCarHistory.getInstance().reset();
        try {
            Files.delete(Paths.get(AbstractParkhausServlet.HISTORY_SAVE_LOCATION));
        }catch(NoSuchFileException e){
        Logger.getLogger("ServletEvenListenerTest").log(Level.INFO, "File not found");
    }

    }

    @Test
    public void contextInitTest(){
        Vehicle vehicle = new Car(AutoGenerator.generateLicense(), MultitonCustomerTypes.getStandardInstance().getName());
        CarPark carPark = new CarPark(10, new Clock("10:00","22:00","13:00"));
        carPark.enter(vehicle,0);
        servletEventListener.contextDestroyed(event);
        servletEventListener.contextInitialized(event);
        Assertions.assertEquals(vehicle.toString(), SingletonCarHistory.getInstance().getCarHistory().get(0).toString());
    }
}
