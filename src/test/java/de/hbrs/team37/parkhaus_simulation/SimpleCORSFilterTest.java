package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import static org.mockito.Mockito.*;

public class SimpleCORSFilterTest {
    HttpServletRequest servletRequest;
    HttpServletResponse servletResponse;
    FilterChain filterChain;
    SimpleCORSFilter simpleCORSFilter = new SimpleCORSFilter();

    @BeforeEach
    public void init(){
        servletRequest = Mockito.spy(HttpServletRequest.class);
        servletResponse = Mockito.spy(HttpServletResponse.class);
        filterChain = Mockito.mock(FilterChain.class);

    }
   // https://stackoverflow.com/questions/23605993/junit-cannot-test-header-information-in-the-response
    @Test
    public void correctParameter() throws ServletException, IOException {
        simpleCORSFilter.doFilter(servletRequest, servletResponse, filterChain);
        verify(servletResponse).setHeader("Access-Control-Allow-Origin", "*");
        verify(servletResponse).setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        verify(servletResponse).setHeader("Access-Control-Max-Age", "3600");
        verify(servletResponse).setHeader("Access-Control-Allow-Headers", "origin, x-requested-with, content-type");
    }
}
