package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SingletonCarHistoryTest {
    SingletonCarHistory s1;
    SingletonCarHistory s2;
    Vehicle v;

    @BeforeEach
    void setUp() {
        s1 = SingletonCarHistory.getInstance();
        s2 = SingletonCarHistory.getInstance();
        v =  AutoGenerator.generateCar("Car");
    }

    @Test
    void getInstance() {
        s1 = SingletonCarHistory.getInstance();
        s2 = SingletonCarHistory.getInstance();
        Assertions.assertEquals(s1,s2);
    }

    @Test
    void getCarHistory() {
        Assertions.assertNotNull(s1.getCarHistory());
    }

    @Test
    void reset() {
        //gefährlich
        s1.getCarHistory().add(v);
        Assertions.assertNotNull(s1.getCarHistory().get(0));
        s1.reset();
        Assertions.assertEquals(0, s1.getCarHistory().size());
    }
}