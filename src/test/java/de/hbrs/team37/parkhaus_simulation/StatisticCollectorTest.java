package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static java.lang.Thread.sleep;
import static org.junit.jupiter.api.Assertions.*;

class StatisticCollectorTest {
    StatisticCollector statisticCollector;
    CarPark carPark;
    Vehicle v1;
    Vehicle v2;
    Vehicle v3;
    List<Vehicle> carHistory;



    @BeforeEach
    void setUp() throws InterruptedException {
        carPark = new CarPark(3, new Clock("10:00", "22:00", "10:00", 0, 1));
        v1 = AutoGenerator.generateCar("Car", "Family");
        v2 = AutoGenerator.generateCar("Car", "Standard");
        v3 = AutoGenerator.generateCar("Car", "Business");
        carPark.enter(v1,0);
        carPark.enter(v2, 1);
        carPark.enter(v3, 2);
        sleep(2000);
        carPark.leave(carPark.randomLeave());
        carPark.leave(carPark.randomLeave());
        carPark.leave(carPark.randomLeave());
        carHistory = SingletonCarHistory.getInstance().getCarHistory();
        statisticCollector = new StatisticCollector(carHistory);
    }

    @AfterEach
    void tearDown() {
        SingletonCarHistory.getInstance().reset();
    }

    @Test
    void moneyFilter() throws IOException {

        statisticCollector.moneyFilter("cartype", MultitonVehicleTypes.getNames());
        assertTrue(statisticCollector.getGeneratedData()[0] > 0);
        assertEquals(0, statisticCollector.getGeneratedData()[1]);
        assertEquals(0, statisticCollector.getGeneratedData()[2]);

        statisticCollector.moneyFilter("customer", MultitonCustomerTypes.getNames());
        assertTrue(statisticCollector.getGeneratedData()[0] > 0);
        assertTrue(statisticCollector.getGeneratedData()[1] > 0);
        assertTrue(statisticCollector.getGeneratedData()[2] > 0);



        statisticCollector.moneyFilter("time_enter", new String[]{"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"});
        assertTrue(statisticCollector.getGeneratedData()[0] > 0);
        assertEquals(0, statisticCollector.getGeneratedData()[1]);
        assertEquals(0, statisticCollector.getGeneratedData()[2]);
        assertEquals(0, statisticCollector.getGeneratedData()[3]);
        assertEquals(0, statisticCollector.getGeneratedData()[4]);
        assertEquals(0, statisticCollector.getGeneratedData()[5]);
        assertEquals(0, statisticCollector.getGeneratedData()[6]);



        statisticCollector.moneyFilter("time_left", new String[]{"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"});
        assertTrue(statisticCollector.getGeneratedData()[0] > 0);
        assertEquals(0, statisticCollector.getGeneratedData()[1]);
        assertEquals(0, statisticCollector.getGeneratedData()[2]);
        assertEquals(0, statisticCollector.getGeneratedData()[3]);
        assertEquals(0, statisticCollector.getGeneratedData()[4]);
        assertEquals(0, statisticCollector.getGeneratedData()[5]);
        assertEquals(0, statisticCollector.getGeneratedData()[6]);
    }

    @Test
    void numberFilter() throws IOException {
        statisticCollector.numberFilter("cartype", MultitonVehicleTypes.getNames());
        assertEquals(Arrays.toString(new double[]{3.0, 0.0, 0.0}), Arrays.toString(statisticCollector.getGeneratedData()));

        statisticCollector.numberFilter("customer", MultitonCustomerTypes.getNames());
        assertEquals(Arrays.toString(new double[]{1.0, 1.0, 1.0}), Arrays.toString(statisticCollector.getGeneratedData()));

        statisticCollector.numberFilter("time_enter", new String[]{"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"});
        assertEquals(
                Arrays.toString(new double[]{3.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}),
                Arrays.toString(statisticCollector.getGeneratedData())
        );

        statisticCollector.numberFilter("time_left", new String[]{"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"});
        assertEquals(
                Arrays.toString(new double[]{3.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}),
                Arrays.toString(statisticCollector.getGeneratedData())
        );
    }
}