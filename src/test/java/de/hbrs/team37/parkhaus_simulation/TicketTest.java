package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mindrot.jbcrypt.BCrypt;
import java.util.concurrent.TimeUnit;

public class TicketTest {
    Ticket ticket;

    @BeforeEach
    public void init() {
        ticket = new Ticket("ABC");
    }

    @Test
    public void timeTest() throws InterruptedException {
        Clock.setSimulationSpeed(1);
        TimeUnit.SECONDS.sleep(6);
        Assertions.assertTrue(5 < ticket.getTime());
    }

    @Test
    public void checkEmptyHash() {
        Assertions.assertTrue(0 < ticket.getHashcode().length());
    }

    @Test
    public void checkCorrectHash() {
        String testString = "ABC" + ticket.getStart();
        Assertions.assertTrue(BCrypt.checkpw(testString, ticket.getHashcode()));
    }

    @Test
    public void checkTimeSafe() throws InterruptedException {
        ticket.setTimeLeft("Day");
        double ticketTime = ticket.getTime();
        TimeUnit.SECONDS.sleep(5);
        Assertions.assertEquals(ticket.getTime(), ticketTime, 0.1);
    }
}
