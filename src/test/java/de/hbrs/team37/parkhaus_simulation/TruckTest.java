package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TruckTest {
    Truck truck = new Truck("a", "Standard");

    @Test
    void testToString() {
        Assertions.assertEquals("Truck", truck.toString().substring(0,5));
    }
}