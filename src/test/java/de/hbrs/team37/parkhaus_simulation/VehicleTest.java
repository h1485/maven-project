package de.hbrs.team37.parkhaus_simulation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import java.security.SecureRandom;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.jupiter.api.Test;
import java.util.logging.Level;
import java.util.logging.Logger;


public class VehicleTest {
    Vehicle vehicle;
    String license;
    String customer;
    Logger logger = Logger.getLogger("VehicleTest");

    @BeforeEach
    public void init(){
        license = AutoGenerator.generateLicense();
        String[] customerTypes = MultitonCustomerTypes.getNames();
        customer = customerTypes[new SecureRandom().nextInt(customerTypes.length)];
        int random = new SecureRandom().nextInt(3);
        switch (random){
            case 0:
                vehicle = new Car(license,customer);
                break;
            case 1:
                vehicle = new Truck(license,customer);
                break;
            default:
                vehicle = new Bike(license,customer);

        }
    }

    @RepeatedTest(30)
    public void correctLicense(){
        Assertions.assertEquals(license, vehicle.getCarID());
    }

    @RepeatedTest(30)
    public void customerType(){
        Assertions.assertEquals(customer, vehicle.getCustomerType());
    }

    @Test
    public void alwaysHasTicket(){
        Assertions.assertNotNull(vehicle.getTicket());
        vehicle = new Car("", "");
        Assertions.assertNotNull(vehicle.getTicket());
    }

    @Test
    public void unknownCustomertype(){
        customer = "SFKSADL";
        vehicle = new Car(license, customer);
        Assertions.assertEquals(vehicle.getCustomerType(), MultitonCustomerTypes.getStandardInstance().getName());
    }

    @RepeatedTest(30)
    public void correctDiscount(){
        double price = 2.0/60.0;
        if(vehicle instanceof Bike)
            price = 1.0/60.0;
        if(vehicle instanceof Truck)
            price = 15.0/60.0;

        MultitonCustomerTypes customerType = MultitonCustomerTypes.getInstance(customer);

        switch(customerType.getName()){
            case "Standard":
                Assertions.assertEquals(price * MultitonCustomerTypes.getStandardInstance().getDiscount(),
                        vehicle.getRawPrice()); //0% Discount
                break;
            case "Family":
                Assertions.assertEquals(price * MultitonCustomerTypes.getFamilyInstance().getDiscount(),
                        vehicle.getRawPrice()); //30% Discount
                break;
            case "Business":
                Assertions.assertEquals(price * MultitonCustomerTypes.getBusinessInstance().getDiscount(),
                        vehicle.getRawPrice()); // 70% Discount
                break;
            default:
                Assertions.fail();
        }
    }

    @Test
    public void checkCorrectString(){
        vehicle.getTicket().setTimeLeft("");
        logger.log(Level.INFO, vehicle.toString());

        String rules = "(" + MultitonVehicleTypes.getVehicleTypesRegex() + ")," +

                "(\\w{1,3}-\\w{1,2}-\\d{1,4})," +
                "(" + MultitonVehicleTypes.getVehicleTypesRegex() + ")," +
                "(\\w+)," +
                "(\\d+\\.\\d+)," +
                "(\\d+)m,,";

        Pattern pattern = Pattern.compile(rules);
        Matcher matcher = pattern.matcher(vehicle.toString());
        if(matcher.find())
            Assertions.assertEquals(matcher.group(0), vehicle.toString());

        String carType = MultitonVehicleTypes.getCarInstance().getName();
        if(vehicle instanceof Bike)
            carType = MultitonVehicleTypes.getBikeInstance().getName();
        if(vehicle instanceof Truck)
            carType = MultitonVehicleTypes.getTruckInstance().getName();

        String builder = carType + "," + vehicle.getCarID() + "," + vehicle.getCustomerType() +
                "," + vehicle.getTicket().getHashcode().substring(10, 20).toUpperCase() + "," + vehicle.getPrice() + "," + vehicle.getTicket().getTime() +
                "min," + vehicle.getTicket().timeEntered + "," + vehicle.getTicket().timeLeft;
        Assertions.assertEquals(builder, vehicle.toString());
    }

    @Test
    public void emptyEnd() throws InterruptedException {
        CarPark carPark = new CarPark(10, new Clock("10:00", "12:00", "10:00"));
        Vehicle localVehicle = new Car(AutoGenerator.generateLicense(), MultitonCustomerTypes.getStandardInstance().getName());
        carPark.enter(localVehicle, 0);
        String[] emptyEndDecon = localVehicle.storeFormat().split(",");
        String emptyEnd = emptyEndDecon[emptyEndDecon.length-1];
        Assertions.assertNotEquals("", emptyEnd);
        Assertions.assertTrue(Math.abs(Long.parseLong(emptyEnd) - new Date().getTime()) < 1000);
        carPark.leave(0);
        TimeUnit.SECONDS.sleep(5);
        Assertions.assertNotNull(localVehicle.getTicket().end);

    }

}
